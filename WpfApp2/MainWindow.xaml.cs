﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }



        private void one_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.CursorColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.RRLineColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.TitleColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.AxisColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.LabelsColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            spControl.SpectrumColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            double[] data = new double[2000];
            for (int i = 0; i < 2000; i++)
            {
                data[i] = random.NextDouble() * -120;
            }
            spControl.PlotSpectr(25, 3025, data);

        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            double[] xdata = new double[3];
            double[] ydata = new double[3];

            xdata[0] = 35;
            xdata[1] = 40;
            xdata[2] = 45;

            ydata[0] = 0;
            ydata[1] = -10;
            ydata[2] = -20;

            spControl.DrawRSArrows(true, xdata, ydata);

        }


        double[] temp;
        double temp1;
        double temp2;

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            double[] data = new double[1400];
            for (int i = 0; i < data.Count(); i++)
            {
                data[i] = random.NextDouble() * -120;
                if (data[i] > -90) data[i] = -95;
                if (data[i] < -100) data[i] = -95;
            }

            data[random.Next(0, 1400)] = -50;
            data[random.Next(0, 1400)] = -50;

            data[random.Next(0, 1400)] = -120;
            data[random.Next(0, 1400)] = -120;


            spControl.PlotSpectr(spControl.MinVisibleX, spControl.MaxVisibleX, data);


            temp = data;
            temp1 = spControl.MinVisibleX;
            temp2 = spControl.MaxVisibleX;

        }

        private void eleven_Click(object sender, RoutedEventArgs e)
        {
            //spControl.PlotStorage(temp1, temp2, temp);
            //spControl.PlotStorage2(temp1, temp2, temp);

            //spControl.PlotStorage(temp1, temp2, temp);

            //spControl.PlotStorageV1(temp1, temp2, temp);

            //spControl.PlotStorageV2(temp1, temp2, temp);

            temp = new[] { -90d, -91d, -95d, -97d, -80d, -79d, -78d, -75d, -76d, -77d, -79d, -80d, -90d, -95d };

            spControl.PlotStorageV3(temp1, temp2, temp);
        }

        private void twelve_Click(object sender, RoutedEventArgs e)
        {
            spControl.ClearStorage();
        }

        private void thirteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.xRangeLabelVisible = !spControl.xRangeLabelVisible;
        }

        private void fourteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.xAxisLabelVisible = !spControl.xAxisLabelVisible;
        }

        private void fifthteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.yAxisLabelVisible = !spControl.yAxisLabelVisible;
        }

        private void sixteen_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            spControl.AdaptiveThreshold = r.Next(-110, -90);
        }

        bool flag = false;
        private void seventeen_Click(object sender, RoutedEventArgs e)
        {
            flag = !flag;
            spControl.ScanSpeedVisible = flag;
        }

        private void eighteen_Click(object sender, RoutedEventArgs e)
        {
            spControl.GlobalRangeXmin = 100;
        }
    }
}

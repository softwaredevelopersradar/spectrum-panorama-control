﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace SpectrumPanoramaControl
{
    public class Visible : INotifyPropertyChanged
    {
        public Visible()
        {
        }

        public Visible(bool topPanelVisible, bool bottomPanelVisible)
        {
            isTopPanelVisible(topPanelVisible);
            isBottomPanelVisible(bottomPanelVisible);
        }

        private double _topPanelHeight = 27;
        public GridLength TopPanelHeight
        {
            get
            {
                return new GridLength(_topPanelHeight);
            }
            set
            {
                _topPanelHeight = value.Value;
                OnPropertyChanged("TopPanelHeight");
            }
        }

        public bool isTopPanelVisible()
        {
            return (TopPanelHeight == new GridLength(27)) ? true : false;
        }
        public void isTopPanelVisible(bool flag)
        {
            TopPanelHeight = new GridLength((flag) ? 27 : 0);
        }

        private double _bottomPanelHeight = 24;
        public GridLength BottomPanelHeight
        {
            get
            {
                return new GridLength(_bottomPanelHeight);
            }
            set
            {
                _bottomPanelHeight = value.Value;
                OnPropertyChanged("BottomPanelHeight");
            }
        }

        public bool isBottomPanelVisible()
        {
            return (BottomPanelHeight == new GridLength(24)) ? true : false;
        }
        public void isBottomPanelVisible(bool flag)
        {
            BottomPanelHeight = new GridLength((flag) ? 24 : 0);
        }

        private double _firstPanelHeight = 22;
        public GridLength FirstPanelHeight
        {
            get
            {
                return new GridLength(_firstPanelHeight);
            }
            set
            {
                _firstPanelHeight = value.Value;
                OnPropertyChanged("FirstPanelHeight");
            }
        }

        public bool isFirstPanelVisible()
        {
            return (FirstPanelHeight == new GridLength(22)) ? true : false;
        }
        public void isFirstPanelVisible(bool flag)
        {
            FirstPanelHeight = new GridLength((flag) ? 22 : 0);
        }

        private bool _ThresholdVisible = true;
        public bool ThresholdVisible
        {
            get => _ThresholdVisible;
            set
            {
                _ThresholdVisible = value;
                OnPropertyChanged(nameof(ThresholdVisible));
            }
        }

        private bool _CRRXVisible = true;
        public bool CRRXVisible
        {
            get => _CRRXVisible;
            set
            {
                _CRRXVisible = value;
                OnPropertyChanged(nameof(CRRXVisible));
            }
        }

        private bool _CRRX2Visible = false;
        public bool CRRX2Visible
        {
            get => _CRRX2Visible;
            set
            {
                _CRRX2Visible = value;
                OnPropertyChanged(nameof(CRRX2Visible));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Axes;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using Arction.Wpf.SemibindableCharting.Views.ViewXY;

namespace SpectrumPanoramaControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class SPControl : UserControl
    {
        #region Variables
        private ViewModel vm;
        private AreaSeriesPoint[] Values;
        #endregion

        #region EventDeclaration
        public delegate void SimpleDoubleEventHandler(double freqMHz);
        public event SimpleDoubleEventHandler CursorOnFreq;
        public delegate void FreqAndThresEventHandler(double freqMHz, short threshold);
        public delegate void FreqThresAndCRREventHandler(double freqMHz, short threshold, CRR crrx);
        public event FreqAndThresEventHandler FreqOnTargetEvent;
        public event FreqAndThresEventHandler FreqOnRSEvent;
        public event FreqThresAndCRREventHandler FreqOnRSSEvent;
        public delegate void FreqLeftNFreqRightEventHandler(double freqLeftMHz, double freqRightMHz);
        public event FreqLeftNFreqRightEventHandler BandOnRSEvent;

        public enum FrequencyType
        {
            Forbidden = 0,
            Known = 1,
            Important = 2,
        }

        public delegate void DoubleDoubleEventHandler(double startFreq, double endFreq);
        public event DoubleDoubleEventHandler AreaFreqOnBearing;
        public event DoubleDoubleEventHandler AreaFreqOnQBearing;

        public delegate void FreqTypeAndTwoDoubleEventHandler(FrequencyType frequencyType, double startFreq, double endFreq);
        public event FreqTypeAndTwoDoubleEventHandler OnFreqArea;

        public event DoubleDoubleEventHandler RangeChangeEvent;

        public delegate void SimpleIntEventHandler(int value);
        public event SimpleIntEventHandler ThresholdChange;
        #endregion

        #region Constructors
        public SPControl()
        {
            //Set Deployment Key for Arction components             
            //string deploymentKey = "lgCAABW2ij+vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD+BCRGnn7c6dwaDiJovCk5g5nFwvJ+G60VSdCrAJ+jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq/B0dVcthh7ezOUzf1uXfOcEJ377/4rwUTR0VbNTCK601EN6/ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM+Q5vztCEz5k+Luaxs+S+OQD3ELg8+y7a/Dv0OhSQkqMDrR/o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry/tAsPPY26Ff3PDl1ItpFWZCzNS/xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi+VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq+F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9+B0YtxFPNBQs=";
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);

            InitializeComponent();

            vm = new ViewModel();
            DataContext = vm;

            InitStartParams();
            InitComponent();

            CreatePointLineSeriesForStorageV1();
            CreatePointLineSeriesForStorageV2();
            CreateHighLowSeriesForStorageV3();
        }

        // функция смены языка
        public void SetLanguage(string lang)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("XMLTranslation.xml")) xDoc.Load("XMLTranslation.xml");
            else
            {
                switch (lang)
                {
                    case "rus":
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "ru":
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "eng":
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "en":
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "az":
                        MessageBox.Show("Fayl XMLTranslation.xml tapılmadı!", "Səhv!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "azlat":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azkir":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                return;
            }

            SortedList<string, string> Dictionary = new SortedList<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            string SectionName = "Panorama_Translation";
            foreach (XmlNode x1Node in xRoot)
            {
                if (x1Node.Name == SectionName)
                {
                    foreach (XmlNode x2Node in x1Node.ChildNodes)
                    {
                        // получаем атрибут ID
                        if (x2Node.Attributes.Count > 0)
                        {

                            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                            if (attr != null)
                            {
                                foreach (XmlNode childnode in x2Node.ChildNodes)
                                {
                                    // если узел - lang
                                    if (childnode.Name == lang)
                                    {
                                        if (!Dictionary.ContainsKey(attr.Value))
                                            Dictionary.Add(attr.Value, childnode.InnerText);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            renameToolTips(Dictionary);
        }

        //переименование всех контролов при смене языка
        private void renameToolTips(SortedList<string, string> Dictionary)
        {
            foreach (var control in stackPanel.Children)
            {
                if (control is Button)
                {
                    var b = (Button)control;

                    if (Dictionary.ContainsKey(b.Name + ".ToolTip"))
                        b.ToolTip = Dictionary[b.Name + ".ToolTip"];
                }
                if (control is ToggleButton)
                {
                    var t = (ToggleButton)control;

                    if (Dictionary.ContainsKey(t.Name + ".ToolTip"))
                        t.ToolTip = Dictionary[t.Name + ".ToolTip"];
                }
            }

            var trans = (Translations)this.Resources["translate"];
            trans.MHz = (Dictionary.ContainsKey("MHz")) ? Dictionary["MHz"] : "МГц";
            trans.dB = (Dictionary.ContainsKey("dB")) ? Dictionary["dB"] : "дБ";

            MHz = (Dictionary.ContainsKey("MHz")) ? Dictionary["MHz"] : "МГц";
            dB = (Dictionary.ContainsKey("dB")) ? Dictionary["dB"] : "дБ";

            UpdateCursorLabel();
            UpdateBandValue();
            UpdateThresValue();
        }


        private void InitStartParams()
        {
            _GlobalNumberOfBands = 100;
            _GlobalBandWidthMHz = 30.0;
            _GlobalRangeXmin = 25;
            _GlobalRangeXmax = _GlobalRangeXmin + _GlobalNumberOfBands * _GlobalBandWidthMHz;
            _GlobalRangeYmax = 0;
            _GlobalRangeYmin = -120;

            _Threshold = -80;

            aresSeries.BaseValue = _GlobalRangeYmin;

            //SpectrumColor = Colors.Lime;
            //SpectrumColor = Colors.LimeGreen;
            //SpectrumColor = Colors.Cyan;

            //SpectrumColor = Color.FromRgb(36, 240, 166); // зелёный от Ангелины, только чёт не тот

            //SpectrumColor = Color.FromRgb(12, 187, 214); // светло-голубой
            //SpectrumColor = Color.FromRgb(19, 159, 188); // голубой
            //SpectrumColor = Color.FromRgb(26, 132, 162); //темно голубой

            band.Visible = false;
        }

        private string MHz = "МГц";

        private string thres = "Th";
        private string dB = "дБ";

        private void InitComponent()
        {
            tbThres.Text = String.Format(thres + " = " + "{0} " + dB, _Threshold.ToString("F0"));

            ScanSpeed = 2.0;
            ScanSpeed2 = 2.0;

            GenerateCustomTicks(ref xAxis);

            //nBar.ClickOnLeft += NBar_ClickOnLeft;
            //nBar.ClickOnRight += NBar_ClickOnRight; ;
            //nBar.Ready += NBar_Ready;
        }
        #endregion

        #region EventHandler

        public void NBar_ClickOnLeft(int Index)
        {
            outRangeFromIndex(out double startValue, out double endValue, Index);
            xAxis.SetRange(startValue, endValue);
        }

        //private void NBar_ClickOnLeft(object sender, NBarControl.NBar.SimpleIntEventArgs e)
        //{
        //    outRangeFromIndex(out double startValue, out double endValue, e.Index);
        //    xAxis.SetRange(startValue, endValue);
        //}

        public void NBar_ClickOnRight(int StartIndex, int EndIndex)
        {
            outRangeFromIndex(out double startValue, out double endValue, StartIndex, EndIndex);
            xAxis.SetRange(startValue, endValue);
        }

        //private void NBar_ClickOnRight(object sender, NBarControl.NBar.DoubleIntEventArgs e)
        //{
        //    outRangeFromIndex(out double startValue, out double endValue, e.StartIndex, e.EndIndex);
        //    xAxis.SetRange(startValue, endValue);
        //}

        public class DoubleIntEventArgs
        {
            public int StartIndex;
            public int EndIndex;
            public DoubleIntEventArgs(int startIndex, int endIndex)
            {
                StartIndex = startIndex;
                EndIndex = endIndex;
            }
        }

        public delegate void SendToNBarDrawActiveRectangle(object sender, DoubleIntEventArgs e);
        public event SendToNBarDrawActiveRectangle SendIndexesToDrawActiveRectangleOnNbar;

        public void NBar_Ready()
        {
            outBandPictureBoxIndex(_GlobalRangeXmin, _GlobalRangeXmax, out int startIndex, out int endIndex);

            SendIndexesToDrawActiveRectangleOnNbar?.Invoke(this, new DoubleIntEventArgs(startIndex, endIndex));
            //nBar.DrawActiveRectangle(startIndex, endIndex);
        }

        public void CenterFreq(double FreqMHz, double BandWidthMHz)
        {
            SetXRange(FreqMHz - BandWidthMHz / 2d, FreqMHz + BandWidthMHz / 2d);
            outBandPictureBoxIndex(FreqMHz - BandWidthMHz / 2d, FreqMHz + BandWidthMHz / 2d, out int startIndex, out int endIndex);
            SendIndexesToDrawActiveRectangleOnNbar?.Invoke(this, new DoubleIntEventArgs(startIndex, endIndex));
        }

        //private void NBar_Ready(object sender, EventArgs e)
        //{
        //    outBandPictureBoxIndex(_GlobalRangeXmin, _GlobalRangeXmax, out int startIndex, out int endIndex);
        //    nBar.DrawActiveRectangle(startIndex, endIndex);
        //}

        #endregion

        #region Properties

        private double _GlobalRangeXmin;
        public double GlobalRangeXmin
        {
            get { return _GlobalRangeXmin; }
            set
            {
                if (_GlobalRangeXmin != value)
                {
                    _GlobalRangeXmin = value;
                    _ReCalc();
                }
            }
        }

        private double _GlobalRangeXmax = 0;
        private double GlobalRangeXmax
        {
            get { return _GlobalRangeXmax; }
            set { if (_GlobalRangeXmax != value) _GlobalRangeXmax = value; }
        }

        private double _GlobalRangeYmin = -120;
        public double GlobalRangeYmin
        {
            get { return _GlobalRangeYmin; }
            set
            {
                if (_GlobalRangeYmin != value)
                {
                    _GlobalRangeYmin = value;
                    SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
                }
            }
        }

        private double _GlobalRangeYmax = 0;
        public double GlobalRangeYmax
        {
            get { return _GlobalRangeYmax; }
            set
            {
                if (_GlobalRangeYmax != value)
                {
                    _GlobalRangeYmax = value;
                    SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
                }
            }
        }

        private int _GlobalNumberOfBands;
        public int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                if (_GlobalNumberOfBands != value)
                {
                    _GlobalNumberOfBands = value;
                    _ReCalc();
                }
            }
        }

        private double _GlobalBandWidthMHz;
        public double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                if (_GlobalBandWidthMHz != value)
                {
                    _GlobalBandWidthMHz = value;
                    _ReCalc();
                }
            }
        }

        private int _DotsPerBand;
        public int DotsPerBand
        {
            get { return _DotsPerBand; }
            set { if (_DotsPerBand != value) _DotsPerBand = value; }
        }

        public double MinVisibleX
        {
            get { return xAxis.Minimum; }
            set
            {
                if (xAxis.Minimum != value)
                {
                    if ((value >= _GlobalRangeXmin) && (value < _GlobalRangeXmax) && (value < xAxis.Maximum))
                        xAxis.Minimum = value;
                }
            }
        }
        public double MaxVisibleX
        {
            get { return xAxis.Maximum; }
            set
            {
                if (xAxis.Maximum != value)
                {
                    if ((value > _GlobalRangeXmin) && (value <= _GlobalRangeXmax) && (value > xAxis.Minimum))
                        xAxis.Maximum = value;
                }
            }
        }
        public double MinVisibleY
        {
            get { return yAxis.Minimum; }
            set
            {
                if (yAxis.Minimum != value)
                {
                    if ((value >= _GlobalRangeYmin) && (value < _GlobalRangeYmax) && (value < yAxis.Maximum))
                        yAxis.Minimum = value;
                }
            }
        }
        public double MaxVisibleY
        {
            get { return yAxis.Maximum; }
            set
            {
                if (yAxis.Maximum != value)
                {
                    if ((value > _GlobalRangeYmin) && (value <= _GlobalRangeYmax) && (value > yAxis.Minimum))
                        yAxis.Maximum = value;
                }
            }
        }

        public string xAxisLabel
        {
            get { return xAxis.Title.Text; }
            set
            {
                if (xAxis.Title.Text != value)
                    xAxis.Title.Text = value;
            }
        }
        public string yAxisLabel
        {
            get { return yAxis.Title.Text; }
            set
            {
                if (yAxis.Title.Text != value)
                    yAxis.Title.Text = value;
            }
        }

        public bool yAxisLabelVisible
        {
            get { return yAxis.Title.Visible; }
            set
            {
                if (yAxis.Title.Visible != value)
                {
                    yAxis.Title.Visible = value;
                    chart.ViewXY.Margins = newMarginsY(chart.ViewXY.Margins, value);
                }
            }
        }

        public bool xAxisLabelVisible
        {
            get { return xAxis.Title.Visible; }
            set
            {
                if (xAxis.Title.Visible != value)
                {
                    xAxis.Title.Visible = value;
                    chart.ViewXY.Margins = newMarginsX(chart.ViewXY.Margins, value);
                }
            }
        }

        private Thickness newMarginsX(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left, Margins.Top, Margins.Right, Margins.Bottom + ((value) ? 15 : -15));
        }

        private Thickness newMarginsX2(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left, Margins.Top, Margins.Right, Margins.Bottom + ((value) ? 25 : -25));
        }

        private Thickness newMarginsY(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left + ((value) ? 20 : -20), Margins.Top, Margins.Right, Margins.Bottom);
        }

        public bool xRangeLabelVisible
        {
            get { return xAxis.LabelsVisible; }
            set
            {
                if (xAxis.LabelsVisible != value)
                {
                    xAxis.LabelsVisible = value;
                    chart.ViewXY.Margins = newMarginsX2(chart.ViewXY.Margins, value);
                }
            }
        }

        private int _Threshold;
        public int Threshold
        {
            get { return _Threshold; }
            set
            {
                if (_Threshold != value)
                {
                    if (value <= _GlobalRangeYmax && value >= _GlobalRangeYmin)
                    {
                        _Threshold = value;
                        DispatchIfNecessary(() =>
                        {
                            RRLine.Value = value;
                        });
                    }
                }

            }

        }

        public int AdaptiveThreshold
        {
            get { return (int)AdLine.Value; }
            set
            {
                if (AdLine.Value != value)
                {
                    AdLine.Value = value;
                }
            }

        }


        private bool _ScanSpeedVisible;
        public bool ScanSpeedVisible
        {
            get { return _ScanSpeedVisible; }
            set
            {
                if (_ScanSpeedVisible != value)
                {
                    _ScanSpeedVisible = value;
                    tbScanSpeed.Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
                    tbScanSpeed2.Visibility = (value) ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        private double _ScanSpeed = 0.0;
        public double ScanSpeed
        {
            get => _ScanSpeed;
            set
            {
                if (_ScanSpeed != value)
                {
                    _ScanSpeed = value;
                    tbScanSpeed.Text = String.Format("{0} = {1} {2}{3}{4}", "Vsr", (_ScanSpeed).ToString("F1"), "ГГц", "/", "с");
                }
            }
        }
        private double _ScanSpeed2 = 0.0;
        public double ScanSpeed2
        {
            get => _ScanSpeed2;
            set
            {
                if (_ScanSpeed2 != value)
                {
                    _ScanSpeed2 = value;
                    tbScanSpeed2.Text = String.Format("{0} = {1} {2}{3}{4}", "Vsu", (_ScanSpeed2).ToString("F2"), "ГГц", "/", "с");
                }
            }
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        public bool topPanelVisible
        {
            get { return vm.V.isTopPanelVisible(); }
            set
            {
                vm.V.isTopPanelVisible(value);
            }
        }
        public bool firstPanelVisible
        {
            get { return vm.V.isFirstPanelVisible(); }
            set
            {
                vm.V.isFirstPanelVisible(value);
            }
        }
        private bool bottomPanelVisible
        {
            get { return vm.V.isBottomPanelVisible(); }
            set
            {
                vm.V.isBottomPanelVisible(value);
            }
        }

        public double cursorX
        {
            get { return CursorX.ValueAtXAxis; }
            set
            {
                if (CursorX.ValueAtXAxis != value)
                    CursorX.ValueAtXAxis = value;

            }
        }
        public double cursorY
        {
            get { return CursorY.Value; }
            set
            {
                if (CursorY.Value != value)
                    CursorY.Value = value;
            }
        }

        public double ChartAWidth
        {
            get { return chart.ActualWidth; }
        }
        public double ChartWidth
        {
            get { return chart.Width; }
        }
        public double ChartAHeight
        {
            get { return chart.ActualHeight; }
        }
        public double ChartHeight
        {
            get { return chart.Height; }
        }

        private Color _SpectrumColor;
        public Color SpectrumColor
        {
            get { return _SpectrumColor; }
            set
            {
                _SpectrumColor = value;
                pointSeries.LineStyle.Color = value;
            }
        }

        private Color _CursorColor;
        public Color CursorColor
        {
            get { return _CursorColor; }
            set
            {
                _CursorColor = value;
                CursorX.LineStyle.Color = value;
                CursorY.LineStyle.Color = value;
            }
        }

        private Color _RRLineColor;
        public Color RRLineColor
        {
            get { return _RRLineColor; }
            set
            {
                _RRLineColor = value;
                RRLine.LineStyle.Color = value;
            }
        }

        private Color _TitleColor;
        public Color TitleColor
        {
            get { return _TitleColor; }
            set
            {
                _TitleColor = value;
                xAxis.Title.Color = value;
                yAxis.Title.Color = value;
            }
        }

        private Color _AxisColor;
        public Color AxisColor
        {
            get { return _AxisColor; }
            set
            {
                _AxisColor = value;
                xAxis.AxisColor = value;
                yAxis.AxisColor = value;
                chart.ViewXY.GraphBorderColor = value;
            }
        }

        private Color _LabelsColor;
        public Color LabelsColor
        {
            get { return _LabelsColor; }
            set
            {
                _LabelsColor = value;
                xAxis.LabelsColor = value;
                yAxis.LabelsColor = value;

                yAxis.MajorDivTickStyle.Color = value;
                yAxis.MinorDivTickStyle.Color = value;
                yAxis.MajorGrid.Color = value;

                xAxis.MajorDivTickStyle.Color = value;
                xAxis.MinorDivTickStyle.Color = value;

                for (int i = 0; i < xAxis.CustomTicks.Count(); i++)
                {
                    xAxis.CustomTicks[i].Color = value;
                }

                xAxis.InvalidateCustomTicks();
            }
        }

        private Color _BackGround;
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                _BackGround = value;

                chart.ChartBackground.Color = value;
                //chart.ChartBackground.GradientFill = GradientFill.Solid;
            }
        }

        public Visibility vrbMHz
        {
            get { return rbMHz.Visibility; }
            set
            {
                rbMHz.Visibility = value;
            }
        }

        private bool _LogXAxes = false;
        public bool LogXAxes
        {
            get => _LogXAxes;
            set
            {
                _LogXAxes = value;
                xAxis.ScaleType = (value) ? ScaleType.Logarithmic : ScaleType.Linear;
            }
        }

        private bool _LogYAxes = false;
        public bool LogYAxes
        {
            get => _LogYAxes;
            set
            {
                _LogYAxes = value;
                yAxis.ScaleType = (value) ? ScaleType.Logarithmic : ScaleType.Linear;
            }
        }

        private bool _ZoomToFitX = false;
        public bool ZoomToFitX
        {
            get => _ZoomToFitX;
            set
            {
                _ZoomToFitX = value;
                if (!value)
                {
                    SetXRange(_GlobalRangeXmin, _GlobalRangeXmax);
                }
            }
        }

        private bool _ZoomToFitY = false;
        public bool ZoomToFitY
        {
            get => _ZoomToFitY;
            set
            {
                _ZoomToFitY = value;
                if (!value)
                {
                    SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
                }
            }
        }

        public bool ThresholdVisible
        {
            get { return vm.V.ThresholdVisible; }
            set
            {
                vm.V.ThresholdVisible = value;
                RRLine.Visible = value;
            }
        }

        public bool CRRXVisible
        {
            get { return vm.V.CRRXVisible; }
            set
            {
                vm.V.CRRXVisible = value;
            }
        }

        public bool CRRX2Visible
        {
            get { return vm.V.CRRX2Visible; }
            set
            {
                vm.V.CRRX2Visible = value;
            }
        }
        #endregion

        #region Public Metods

        public void ChangeXRange(double startFreq, double endFreq)
        {
            xAxis.SetRange(startFreq, endFreq);
        }

        public void PlotSpectr(int[] dataArray)
        {
            PlotSpectr(dataArray.Select(x => Convert.ToDouble(x)).ToArray());
        }
        public void PlotSpectr(double startFreq, double endFreq, int[] dataArray)
        {
            PlotSpectr(startFreq, endFreq, dataArray.Select(x => Convert.ToDouble(x)).ToArray());
        }
        public void PlotSpectr(double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var min = chart.ViewXY.XAxes[0].Minimum;
            var max = chart.ViewXY.XAxes[0].Maximum;

            var data = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = min + i * (max - min) / pointCounter;
                data[i].Y = dataArray[i];
            }

            pointSeries.Points = data;
        }
        public void PlotSpectr0(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var data = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                data[i].Y = dataArray[i];
            }

            pointSeries.Points = data;
        }

        public void PlotSpectr(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var data = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = startFreq + i * (endFreq - startFreq) / (pointCounter - 1);
                data[i].Y = dataArray[i];
            }
            chart.BeginUpdate();
            pointSeries.Points = data;
            if (ZoomToFitX) SetXRange(startFreq, endFreq);
            if (ZoomToFitY) SetYRange(dataArray.Min(), dataArray.Max());
            chart.EndUpdate();
        }

        double prevStartFreq = 0;
        double prevEndFreq = 0;

        public void PlotStorage(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var TempValues = new AreaSeriesPoint[pointCounter];

            for (int i = 0; i < TempValues.Count(); i++)
            {
                TempValues[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                TempValues[i].Y = dataArray[i];
            }

            if (prevStartFreq == startFreq && prevEndFreq == endFreq)
            {
                //объединить данные
                Values = Values.Concat(TempValues).ToArray(); ;
            }
            else
            {
                Values = TempValues;
            }

            aresSeries.Points = Values;

            prevStartFreq = startFreq;
            prevEndFreq = endFreq;
        }

        /*
        List<AreaSeriesPoint[]> ls = new List<AreaSeriesPoint[]>();

        public void PlotStorage(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            //if (Values?.Count() != pointCounter)
            {
                Values = new AreaSeriesPoint[pointCounter];

                for (int i = 0; i < Values.Count(); i++)
                {
                    Values[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                    //Values[i].Y = -120;
                    Values[i].Y = dataArray[i];
                }
            }

            if (prevStartFreq == startFreq && prevEndFreq == endFreq)
            {
                for (int i = 0; i < Values.Count(); i++)
                {
                    if (Values[i].Y < dataArray[i]) Values[i].Y = dataArray[i];
                }
            }

            ls.Add(Values);

            Values = ConcatArrASP(ls);

            aresSeries.Points = Values;

            prevStartFreq = startFreq;
            prevEndFreq = endFreq;
        }

        public void PlotStorage2(double startFreq, double endFreq, double[] dataArray)
        {

            int pointCounter = dataArray.Length;

            //if (Values?.Count() != pointCounter)
            {
                Values = new AreaSeriesPoint[pointCounter];

                for (int i = 0; i < Values.Count(); i++)
                {
                    Values[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                    //Values[i].Y = -120;
                    Values[i].Y = dataArray[i];
                }
            }

            if (prevStartFreq == startFreq && prevEndFreq == endFreq)
            {
                for (int i = 0; i < Values.Count(); i++)
                {
                    if (Values[i].Y < dataArray[i]) Values[i].Y = dataArray[i];
                }
            }

            Values = IntersectionValues2(Values);

            aresSeries.Points = Values;

            prevStartFreq = startFreq;
            prevEndFreq = endFreq;

        }

        public void PlotStorage2_1(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var TempValues = new AreaSeriesPoint[pointCounter];

            for (int i = 0; i < TempValues.Count(); i++)
            {
                TempValues[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                TempValues[i].Y = dataArray[i];
            }

            if (prevStartFreq == startFreq && prevEndFreq == endFreq)
            {
                //объединить данные
                Values = Values.Concat(TempValues).ToArray(); ;
            }
            else
            {
                Values = TempValues;
            }

            aresSeries.Points = Values;

            prevStartFreq = startFreq;
            prevEndFreq = endFreq;
        }

        public void PlotStorage3(double startFreq, double endFreq, double[] dataArray)
        {

            int pointCounter = dataArray.Length;

            //if (Values?.Count() != pointCounter)
            {
                Values = new AreaSeriesPoint[pointCounter];

                for (int i = 0; i < Values.Count(); i++)
                {
                    Values[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                    //Values[i].Y = -120;
                    Values[i].Y = dataArray[i];
                }
            }

            Values = IntersectionValues(Values);

            aresSeries.Points = Values;

            prevStartFreq = startFreq;
            prevEndFreq = endFreq;

        }

        public void PlotStorage4(double startFreq, double endFreq, double[] dataArray)
        {

            int pointCounter = dataArray.Length;

            //if (Values?.Count() != pointCounter)
            {
                Values = new AreaSeriesPoint[pointCounter];

                for (int i = 0; i < Values.Count(); i++)
                {
                    Values[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                    //Values[i].Y = -120;
                    Values[i].Y = dataArray[i];
                }
            }

            Values = UnionValues(Values);

            aresSeries.Points = Values;

            prevStartFreq = startFreq;
            prevEndFreq = endFreq;

        }

        List<AreaSeriesPoint> LSP = new List<AreaSeriesPoint>();
        private AreaSeriesPoint[] IntersectionValues(AreaSeriesPoint[] values)
        {
            if (LSP.Count != 0)
            {
                double oldXMin = LSP[0].X; ;
                double oldXMax = LSP[LSP.Count - 1].X;
                double newXMin = values[0].X;
                double newXMax = values[values.Length-1].X;

                if(newXMin< oldXMin && newXMax < oldXMin)
                {
                    //1
                    LSP.InsertRange(0, values);
                }

                if (newXMin < oldXMin && newXMax > oldXMin && newXMax < oldXMax)
                {
                    //2
                    LSP.InsertRange(0, values);
                }

                if (newXMin > oldXMin && newXMin < oldXMax && newXMax > oldXMax)
                {
                    //4
                    LSP.InsertRange(LSP.Count - 1, values);
                }

                if (newXMin > oldXMax && newXMax > oldXMax)
                {
                    //5
                    LSP.InsertRange(LSP.Count - 1, values);
                }

                return LSP.ToArray();
            }
            else
            {
                LSP.AddRange(values);
                return LSP.ToArray();
            }
        }

        private AreaSeriesPoint[] IntersectionValues2(AreaSeriesPoint[] values)
        {
            if (LSP.Count != 0)
            {
                LSP.InsertRange(0, values);
                return LSP.ToArray();
            }
            else
            {
                LSP.AddRange(values);
                return LSP.ToArray();
            }
        }

        private AreaSeriesPoint[] valuis = new AreaSeriesPoint[0];
        private AreaSeriesPoint[] ConcatValues(AreaSeriesPoint[] values)
        {
            var result = valuis.Concat(values);
            valuis = result.ToArray();
            return valuis;
        }
        private AreaSeriesPoint[] UnionValues(AreaSeriesPoint[] values)
        {
            var result = valuis.Union(values);
            valuis = result.ToArray();
            return valuis;
        }

        public AreaSeriesPoint[] ConcatArrASP(List<AreaSeriesPoint[]> ls)
        {
            AreaSeriesPoint[] result = new AreaSeriesPoint[ls.Sum(a => a.Length)];
            int offset = 0;

            for (int i=0; i<ls.Count(); i++)
            {
                ls[i].CopyTo(result, offset);
                offset += ls[i].Length;
            }

            return result;
        }
        */

        public void ClearStorage()
        {
            aresSeries.Clear();
            //LSP.Clear();
            ClearPersistentLayerV2();
            chart.ViewXY.HighLowSeries[0].Clear();
        }

        // Layer for historic data.
        PersistentSeriesRenderingLayer _historicDataLayer;

        private void CreatePointLineSeriesForStorageV1()
        {
            //Disable rendering, strongly recommended before updating chart properties
            chart.BeginUpdate();

            //PointLineSeries for historing trace. This series data is rewritten over and over again, and rendered only 
            //to the persistent layer. 
            PointLineSeries seriesHistoricTrace = new PointLineSeries(chart.ViewXY, chart.ViewXY.XAxes[0], chart.ViewXY.YAxes[0]);
            seriesHistoricTrace.Visible = false;
            seriesHistoricTrace.Title.Text = "Historic";
            seriesHistoricTrace.LineStyle.Width = 1;
            seriesHistoricTrace.LineVisible = true;
            seriesHistoricTrace.PointsVisible = false;
            seriesHistoricTrace.LineStyle.Color = Colors.Lime;
            seriesHistoricTrace.PointStyle.Shape = Arction.Wpf.SemibindableCharting.Shape.Cross;
            seriesHistoricTrace.PointStyle.Width = 15;
            seriesHistoricTrace.PointStyle.Height = 15;
            seriesHistoricTrace.PointStyle.Angle = 45;
            seriesHistoricTrace.PointStyle.Color1 = Colors.Lime;
            chart.ViewXY.PointLineSeries.Add(seriesHistoricTrace);

            //Allow chart rendering
            chart.EndUpdate();
        }

        private void ClearPersistentLayerV1()
        {
            if (_historicDataLayer != null)
            {
                Color color = chart.ViewXY.GraphBackground.Color;
                _historicDataLayer.Clear(System.Drawing.Color.FromArgb(0, color.R, color.G, color.B));
            }
        }

        public void PlotStorageV1(double startFreq, double endFreq, double[] dataArray)
        {
            chart.BeginUpdate();

            if (_historicDataLayer == null)
            {
                _historicDataLayer = new PersistentSeriesRenderingLayer(chart.ViewXY, chart.ViewXY.XAxes[0]);
                ClearPersistentLayerV1();
            }

            double[] data = dataArray;

            int count = data.Length;
            double minX = startFreq;
            double maxX = endFreq;

            double xInterval = (maxX - minX) / (double)(count - 1);
            SeriesPoint[] points = new SeriesPoint[count];

            for (int i = 0; i < count; i++)
            {
                points[i].X = minX + xInterval * (double)i;
                points[i].Y = data[i];
            }

            PointLineSeries seriesHistoricTrace = chart.ViewXY.PointLineSeries[1];
            seriesHistoricTrace.MouseInteraction = false;
            seriesHistoricTrace.Points = points;

            //Fade older data away, a little bit, alpha in range 0.95 - 1.0
            double decay = 0.95;
            _historicDataLayer.MultiplyAlpha(decay);

            //Render the series into the layer 
            _historicDataLayer.RenderSeries(seriesHistoricTrace);

            //Update chart, with the updated layer 
            chart.EndUpdate();
        }


        private PersistentSeriesRenderingLayer _historicDataLayer2;

        private void CreatePointLineSeriesForStorageV2(bool flag = true)
        {
            bool AreaVisible = !flag;
            bool HighSVisible = flag;

            //Disable rendering, strongly recommended before updating chart properties
            chart.BeginUpdate();

            //Area and High-low for historing trace. The data of these series is rewritten over and over again, and rendered only 
            //to the persistent layer. 
            AreaSeries historicTraceArea = new AreaSeries(chart.ViewXY, chart.ViewXY.XAxes[0], chart.ViewXY.YAxes[0]);
            historicTraceArea.Visible = AreaVisible;
            historicTraceArea.Title.Text = "Historic area";
            historicTraceArea.LineStyle.Width = 1;
            historicTraceArea.LineVisible = true;
            historicTraceArea.BaseValue = -120;

            Color color = Colors.Orange;
            historicTraceArea.Fill.Color = Color.FromArgb(100, color.R, color.G, color.B);

            historicTraceArea.Fill.GradientFill = GradientFill.Solid;
            historicTraceArea.Fill.Style = RectFillStyle.ColorOnly;

            color = Colors.Orange;
            historicTraceArea.LineStyle.Color = Color.FromArgb(255, color.R, color.G, color.B);

            historicTraceArea.PointsVisible = false;
            if (AreaVisible)
                chart.ViewXY.AreaSeries.Add(historicTraceArea);

            HighLowSeries historicTraceHighLow = new HighLowSeries(chart.ViewXY, chart.ViewXY.XAxes[0], chart.ViewXY.YAxes[0]);
            historicTraceHighLow.Visible = HighSVisible;
            historicTraceHighLow.Title.Text = "Historic area";
            historicTraceHighLow.LineStyle.Width = 1;
            historicTraceHighLow.LineVisibleHigh = true;
            historicTraceHighLow.LineVisibleLow = true;

            color = Colors.Orange;
            historicTraceHighLow.Fill.Color = Color.FromArgb(100, color.R, color.G, color.B);

            historicTraceHighLow.Fill.GradientFill = GradientFill.Solid;
            historicTraceHighLow.Fill.Style = RectFillStyle.ColorOnly;

            color = Colors.Orange;
            historicTraceHighLow.LineStyleHigh.Color = Color.FromArgb(255, color.R, color.G, color.B);
            historicTraceHighLow.LineStyleLow.Color = Color.FromArgb(255, color.R, color.G, color.B);

            historicTraceHighLow.PointsVisible = false;
            if (HighSVisible)
                chart.ViewXY.HighLowSeries.Add(historicTraceHighLow);

            //Allow chart rendering
            chart.EndUpdate();
        }

        private void ClearPersistentLayerV2()
        {
            if (_historicDataLayer2 != null)
            {
                Color colorBackground = chart.ViewXY.GraphBackground.Color;
                System.Drawing.Color color = System.Drawing.Color.FromArgb(0, colorBackground.R, colorBackground.G, colorBackground.B);
                _historicDataLayer2.Clear(color);
            }
        }

        public void PlotStorageV2(double startFreq, double endFreq, double[] dataArray)
        {
            chart.BeginUpdate();

            if (_historicDataLayer2 == null)
            {
                _historicDataLayer2 = new PersistentSeriesRenderingLayer(chart.ViewXY, chart.ViewXY.XAxes[0]);
                ClearPersistentLayerV2();
            }

            double[] data = dataArray;

            int count = data.Length;
            double minX = startFreq;
            double maxX = endFreq;

            double xInterval = (maxX - minX) / (double)(count - 1);

            HighLowSeriesBase historicTrace;
            AreaSeriesPoint[] pointsArea = null;
            HighLowSeriesPoint[] pointsHighLow = null;


            if (chart.ViewXY.AreaSeries.Count == 2 && chart.ViewXY.AreaSeries[1].Visible == true)
            {
                //Use AreaSeries
                pointsArea = new AreaSeriesPoint[count];
                for (int i = 0; i < count; i++)
                {
                    pointsArea[i].X = minX + xInterval * (double)i;
                    pointsArea[i].Y = data[i];
                }
                AreaSeries historicTraceArea = chart.ViewXY.AreaSeries[1];
                historicTraceArea.BaseValue = data.Min<double>();
                historicTraceArea.Points = pointsArea;
                historicTrace = historicTraceArea;
            }
            else
            {
                //Use HighLowSeries 
                pointsHighLow = new HighLowSeriesPoint[count];
                var min = data.Min<double>();
                for (int i = 0; i < count; i++)
                {
                    pointsHighLow[i].X = minX + xInterval * (double)i;
                    pointsHighLow[i].YHigh = data[i];
                    //pointsHighLow[i].YLow = data[i];
                    if (data[i] > _Threshold)
                    {
                        pointsHighLow[i].YLow = min;
                    }
                    else
                    {
                        pointsHighLow[i].YLow = data[i];
                        min = data[i];
                    }
                }
                HighLowSeries historicTraceHighLow = chart.ViewXY.HighLowSeries[0];
                historicTraceHighLow.MouseInteraction = false;
                historicTraceHighLow.Points = pointsHighLow;
                historicTrace = historicTraceHighLow;
            }

            double decay = 0.95;
            //Fade older data away, a little bit, alpha in range 0.95 - 1.0
            _historicDataLayer2.MultiplyAlpha(decay);

            //Render the series into the layer 
            _historicDataLayer2.RenderSeries(historicTrace);

            //Update chart, with the updated layer 
            chart.EndUpdate();

        }

        public void PlotStorageV3(double startFreq, double endFreq, double[] dataArray)
        {
            chart.BeginUpdate();

            if (_historicDataLayer2 == null)
            {
                _historicDataLayer2 = new PersistentSeriesRenderingLayer(chart.ViewXY, chart.ViewXY.XAxes[0]);
                ClearPersistentLayerV2();
            }

            double[] data = dataArray;

            int count = data.Length;
            double minX = startFreq;
            double maxX = endFreq;

            double xInterval = (maxX - minX) / (double)(count - 1);

            HighLowSeriesBase historicTrace;
            AreaSeriesPoint[] pointsArea = null;
            HighLowSeriesPoint[] pointsHighLow = null;


            if (chart.ViewXY.AreaSeries.Count == 2 && chart.ViewXY.AreaSeries[1].Visible == true)
            {
                //Use AreaSeries
                pointsArea = new AreaSeriesPoint[count];
                for (int i = 0; i < count; i++)
                {
                    pointsArea[i].X = minX + xInterval * (double)i;
                    pointsArea[i].Y = data[i];
                }
                AreaSeries historicTraceArea = chart.ViewXY.AreaSeries[1];
                historicTraceArea.BaseValue = data.Min<double>();
                historicTraceArea.Points = pointsArea;
                historicTrace = historicTraceArea;
            }
            else
            {
                //Use HighLowSeries 
                pointsHighLow = new HighLowSeriesPoint[count];
                var min = data.Min<double>();
                double prevmin = min;
                for (int i = 0; i < count; i++)
                {
                    pointsHighLow[i].X = minX + xInterval * (double)i;
                    pointsHighLow[i].YHigh = data[i];

                    if (data[i] > _Threshold)
                    {
                        int k = i;
                        int countmin = 0;

                        if (i - 1 >= 0)
                            if (data[i] >= data[i - 1])
                            {

                                while (data[k] >= data[k - 1] || data[k] > _Threshold)
                                {
                                    k--;
                                    countmin++;
                                    if (k - 1 < 0)
                                        break;
                                }
                                for (int w = i - countmin; w <= i; w++)
                                {
                                    pointsHighLow[w].YLow = data[i - countmin];
                                    prevmin = data[i - countmin];
                                }
                            }
                            else
                            {
                                pointsHighLow[i].YLow = prevmin;
                            }
                    }
                    else
                    {
                        pointsHighLow[i].YLow = data[i];
                        prevmin = data[i];
                    }

                }
                HighLowSeries historicTraceHighLow = chart.ViewXY.HighLowSeries[0];
                historicTraceHighLow.MouseInteraction = false;
                historicTraceHighLow.Points = pointsHighLow;
                historicTrace = historicTraceHighLow;
            }

            double decay = 0.95;
            //Fade older data away, a little bit, alpha in range 0.95 - 1.0
            _historicDataLayer2.MultiplyAlpha(decay);

            //Render the series into the layer 
            _historicDataLayer2.RenderSeries(historicTrace);

            //Update chart, with the updated layer 
            chart.EndUpdate();

        }

        public void PlotStorageV4(double startFreq, double endFreq, double[] dataArray)
        {
            chart.BeginUpdate();

            if (_historicDataLayer2 == null)
            {
                _historicDataLayer2 = new PersistentSeriesRenderingLayer(chart.ViewXY, chart.ViewXY.XAxes[0]);
                ClearPersistentLayerV2();
            }

            double[] data = dataArray;

            int count = data.Length;
            double minX = startFreq;
            double maxX = endFreq;

            double xInterval = (maxX - minX) / (double)(count - 1);

            HighLowSeriesBase historicTrace;
            AreaSeriesPoint[] pointsArea = null;
            HighLowSeriesPoint[] pointsHighLow = null;


            if (chart.ViewXY.AreaSeries.Count == 2 && chart.ViewXY.AreaSeries[1].Visible == true)
            {
                //Use AreaSeries
                pointsArea = new AreaSeriesPoint[count];
                for (int i = 0; i < count; i++)
                {
                    pointsArea[i].X = minX + xInterval * (double)i;
                    pointsArea[i].Y = data[i];
                }
                AreaSeries historicTraceArea = chart.ViewXY.AreaSeries[1];
                historicTraceArea.BaseValue = data.Min<double>();
                historicTraceArea.Points = pointsArea;
                historicTrace = historicTraceArea;
            }
            else
            {
                //Use HighLowSeries 
                pointsHighLow = new HighLowSeriesPoint[count];
                var min = data.Min<double>();
                double prevmin = min;
                for (int i = 0; i < count; i++)
                {
                    pointsHighLow[i].X = minX + xInterval * (double)i;
                    pointsHighLow[i].YHigh = data[i];

                    if (data[i] > _Threshold)
                    {
                        //поиск влево
                        int countminleft = SearchMinLeft(i, data, _Threshold, prevmin);

                        double minvalue = data[i - countminleft];

                        if (minvalue > _Threshold)
                        {
                            //поиск вправо
                            int countminright = SearchMinRight(i, data, _Threshold, prevmin);
                            minvalue = data[i + countminright];
                        }

                        for (int w = i - countminleft; w <= i; w++)
                        {
                            pointsHighLow[w].YLow = minvalue;
                            prevmin = minvalue;
                        }

                    }
                    else
                    {
                        pointsHighLow[i].YLow = data[i];
                        prevmin = data[i];
                    }

                }
                HighLowSeries historicTraceHighLow = chart.ViewXY.HighLowSeries[0];
                historicTraceHighLow.MouseInteraction = false;
                historicTraceHighLow.Points = pointsHighLow;
                historicTrace = historicTraceHighLow;
            }

            double decay = 0.95;
            //Fade older data away, a little bit, alpha in range 0.95 - 1.0
            _historicDataLayer2.MultiplyAlpha(decay);

            //Render the series into the layer 
            _historicDataLayer2.RenderSeries(historicTrace);

            //Update chart, with the updated layer 
            chart.EndUpdate();

        }

        private double _Decay = 0.85;
        public double Decay
        {
            get { return _Decay; }
            set
            {
                if (_Decay != value)
                {
                    _Decay = value;
                }
            }
        }

        public void PlotStorageV5(double startFreq, double endFreq, double[] dataArray)
        {
            chart.BeginUpdate();

            if (_historicDataLayer2 == null)
            {
                _historicDataLayer2 = new PersistentSeriesRenderingLayer(chart.ViewXY, chart.ViewXY.XAxes[0]);
                ClearPersistentLayerV2();
            }

            double[] data = dataArray;

            int count = data.Length;
            double minX = startFreq;
            double maxX = endFreq;

            double xInterval = (maxX - minX) / (double)(count - 1);

            HighLowSeriesBase historicTrace;
            AreaSeriesPoint[] pointsArea = null;
            HighLowSeriesPoint[] pointsHighLow = null;


            if (chart.ViewXY.AreaSeries.Count == 2 && chart.ViewXY.AreaSeries[1].Visible == true)
            {
                //Use AreaSeries
                pointsArea = new AreaSeriesPoint[count];
                for (int i = 0; i < count; i++)
                {
                    pointsArea[i].X = minX + xInterval * (double)i;
                    pointsArea[i].Y = data[i];
                }
                AreaSeries historicTraceArea = chart.ViewXY.AreaSeries[1];
                historicTraceArea.BaseValue = data.Min<double>();
                historicTraceArea.Points = pointsArea;
                historicTrace = historicTraceArea;
            }
            else
            {
                //Use HighLowSeries 
                pointsHighLow = new HighLowSeriesPoint[count];
                for (int i = 0; i < count; i++)
                {
                    pointsHighLow[i].X = minX + xInterval * (double)i;
                    pointsHighLow[i].YHigh = data[i];
                    pointsHighLow[i].YLow = data[i];
                }
                HighLowSeries historicTraceHighLow = chart.ViewXY.HighLowSeries[0];
                historicTraceHighLow.MouseInteraction = false;
                historicTraceHighLow.Points = pointsHighLow;
                historicTrace = historicTraceHighLow;
            }

            //double decay = 0.95;
            //Fade older data away, a little bit, alpha in range 0.95 - 1.0
            _historicDataLayer2.MultiplyAlpha(_Decay);

            //Render the series into the layer 
            _historicDataLayer2.RenderSeries(historicTrace);

            //Update chart, with the updated layer 
            chart.EndUpdate();

        }

        private void CreateHighLowSeriesForStorageV3()
        {
            Color lineColor = Colors.Yellow;

            // Add high-low or area for historical (persistent) data.
            var highLowSeries = new HighLowSeries(chart.ViewXY, chart.ViewXY.XAxes[0], chart.ViewXY.YAxes[0]);
            highLowSeries.Title.Visible = false;

            //highLowSeries.LineVisibleHigh = false;
            //highLowSeries.LineVisibleLow = false;
            highLowSeries.LineVisibleHigh = true;
            highLowSeries.LineStyleHigh.Color = ChartTools.CalcGradient(lineColor, Colors.Black, 50);
            highLowSeries.LineVisibleLow = true;
            highLowSeries.LineStyleLow.Color = ChartTools.CalcGradient(lineColor, Colors.Black, 50);

            highLowSeries.Fill.Color = ChartTools.CalcGradient(lineColor, Colors.Black, 50);
            highLowSeries.Fill.GradientFill = GradientFill.Solid;
            highLowSeries.MouseInteraction = false;
            highLowSeries.PointsVisible = false;

            //highLowSeries.Points = new HighLowSeriesPoint[3000];
            //highLowSeries.Points = highLowSeries.Points.Select(x => x = -120).ToArray();

            chart.ViewXY.HighLowSeries.Add(highLowSeries);
        }


        public void PlotSaveStorage0(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var data = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                data[i].Y = dataArray[i];
            }

            //updateHLS2(data);
            //updateHLS3(data);
            updateHLS4(data);
        }

        public void PlotSaveStorage(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var data = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = startFreq + i * (endFreq - startFreq) / (pointCounter - 1);
                data[i].Y = dataArray[i];
            }

            //updateHLS2(data);
            //updateHLS3(data);
            //updateHLS4(data);

            //updateHLS5(data);
            //updateHLS6(data);

            updateHLS7(data);
        }

        private void updateHLS2(SeriesPoint[] data)
        {
            if (chart.ViewXY.HighLowSeries[1].Points == null || chart.ViewXY.HighLowSeries[1].Points.Count() == 0)
            {
                int pointCounter = data.Length;
                var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                for (int i = 0; i < pointCounter; i++)
                {
                    pointsHighLow[i].X = data[i].X;
                    pointsHighLow[i].YHigh = data[i].Y;
                    pointsHighLow[i].YLow = data[i].Y;
                }
                chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
            }
            else
            {
                int pointCounter = data.Length;
                var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                for (int i = 0; i < pointCounter; i++)
                {
                    pointsHighLow[i] = new HighLowSeriesPoint(chart.ViewXY.HighLowSeries[1].Points[i].X, chart.ViewXY.HighLowSeries[1].Points[i].YHigh, chart.ViewXY.HighLowSeries[1].Points[i].YLow);

                    if (data[i].Y > pointsHighLow[i].YHigh)
                    {
                        pointsHighLow[i].YHigh = data[i].Y;

                    }
                    if (pointsHighLow[i].YLow > data[i].Y)
                    {
                        pointsHighLow[i].YLow = data[i].Y;

                    }
                }
                chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
            }
        }


        List<HighLowSeriesPoint[]> Lhlspoints4HLS = new List<HighLowSeriesPoint[]>();

        private void updateHLS3(SeriesPoint[] data)
        {
            if (chart.ViewXY.HighLowSeries[1].Points == null || chart.ViewXY.HighLowSeries[1].Points.Count() == 0)
            {
                int pointCounter = data.Length;
                var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                for (int i = 0; i < pointCounter; i++)
                {
                    pointsHighLow[i].X = data[i].X;
                    pointsHighLow[i].YHigh = data[i].Y;
                    pointsHighLow[i].YLow = data[i].Y;
                }

                Lhlspoints4HLS.Add(pointsHighLow);

                chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
            }
            else
            {
                var lastListHLSPArray = Lhlspoints4HLS[Lhlspoints4HLS.Count - 1];
                if (lastListHLSPArray.Count() == data.Length &&
                    lastListHLSPArray[0].X == data[0].X && lastListHLSPArray[data.Length - 1].X == data[data.Length - 1].X)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (data[i].Y > lastListHLSPArray[i].YHigh)
                        {
                            lastListHLSPArray[i].YHigh = data[i].Y;

                        }
                        if (lastListHLSPArray[i].YLow > data[i].Y)
                        {
                            lastListHLSPArray[i].YLow = data[i].Y;

                        }
                    }
                    Lhlspoints4HLS[Lhlspoints4HLS.Count - 1] = lastListHLSPArray;
                }
                else
                {
                    int pointCounter = data.Length;
                    var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                    for (int i = 0; i < pointCounter; i++)
                    {
                        pointsHighLow[i].X = data[i].X;
                        pointsHighLow[i].YHigh = data[i].Y;
                        pointsHighLow[i].YLow = data[i].Y;
                    }
                    Lhlspoints4HLS.Add(pointsHighLow);
                }

                if (Lhlspoints4HLS.Count > 60)
                {
                    Lhlspoints4HLS.RemoveAt(0);
                }

                List<HighLowSeriesPoint> lpslpoints = new List<HighLowSeriesPoint>();

                for (int w = 0; w < Lhlspoints4HLS.Count(); w++)
                {
                    lpslpoints.AddRange(Lhlspoints4HLS[w]);
                }

                //chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                chart.BeginUpdate();
                chart.ViewXY.HighLowSeries[1].Points = lpslpoints.ToArray();
                chart.EndUpdate();
            }
        }


        private struct HighLowSeriesPointsPlusDateTime
        {
            public HighLowSeriesPoint[] highLowSeriesPoints;
            public DateTime dateTime;

            public HighLowSeriesPointsPlusDateTime(HighLowSeriesPoint[] highLowSeriesPoints, DateTime dateTime)
            {
                this.highLowSeriesPoints = highLowSeriesPoints;
                this.dateTime = dateTime;
            }
        }

        List<HighLowSeriesPointsPlusDateTime> newLhlspoints4HLS = new List<HighLowSeriesPointsPlusDateTime>();

        private uint _SavePlotStorageTime = 60;
        public uint SavePlotStorageTime
        {
            get { return SavePlotStorageTime; }
            set { if (_SavePlotStorageTime != value) _SavePlotStorageTime = value; }
        }

        private void updateHLS4(SeriesPoint[] data)
        {
            if (chart.ViewXY.HighLowSeries[1].Points == null || chart.ViewXY.HighLowSeries[1].Points.Count() == 0)
            {
                int pointCounter = data.Length;
                var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                for (int i = 0; i < pointCounter; i++)
                {
                    pointsHighLow[i].X = data[i].X;
                    pointsHighLow[i].YHigh = data[i].Y;
                    pointsHighLow[i].YLow = data[i].Y;
                }

                HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                newLhlspoints4HLS.Add(temp);

                chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
            }
            else
            {
                var lastnewListHLSPArray = newLhlspoints4HLS[newLhlspoints4HLS.Count - 1].highLowSeriesPoints;
                if (lastnewListHLSPArray.Count() == data.Length &&
                    lastnewListHLSPArray[0].X == data[0].X && lastnewListHLSPArray[data.Length - 1].X == data[data.Length - 1].X)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        //if (data[i].Y > lastnewListHLSPArray[i].YHigh)
                        //{
                        //    lastnewListHLSPArray[i].YHigh = data[i].Y;

                        //}
                        //if (lastnewListHLSPArray[i].YLow > data[i].Y)
                        //{
                        //    lastnewListHLSPArray[i].YLow = data[i].Y;

                        //}
                        if (data[i].Y > lastnewListHLSPArray[i].YHigh)
                        {
                            lastnewListHLSPArray[i].YHigh = data[i].Y;
                            lastnewListHLSPArray[i].YLow = data[i].Y;
                        }
                    }
                    newLhlspoints4HLS[newLhlspoints4HLS.Count() - 1] = new HighLowSeriesPointsPlusDateTime(lastnewListHLSPArray, DateTime.Now);
                }
                else
                {
                    int pointCounter = data.Length;
                    var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                    for (int i = 0; i < pointCounter; i++)
                    {
                        pointsHighLow[i].X = data[i].X;
                        pointsHighLow[i].YHigh = data[i].Y;
                        pointsHighLow[i].YLow = data[i].Y;
                    }
                    HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                    newLhlspoints4HLS.Add(temp);
                }

                for (int i = 0; i < newLhlspoints4HLS.Count(); i++)
                {
                    var diff = DateTime.Now - newLhlspoints4HLS[i].dateTime;
                    if (diff.TotalSeconds > _SavePlotStorageTime)
                        newLhlspoints4HLS.RemoveAt(i);
                }

                List<HighLowSeriesPoint> lpslpoints = new List<HighLowSeriesPoint>();

                for (int w = 0; w < newLhlspoints4HLS.Count(); w++)
                {
                    lpslpoints.AddRange(newLhlspoints4HLS[w].highLowSeriesPoints);
                }

                //chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                chart.BeginUpdate();
                try
                {
                    chart.ViewXY.HighLowSeries[1].Points = lpslpoints.ToArray();
                }
                catch
                {
                    chart.ViewXY.HighLowSeries[1].Clear();
                    newLhlspoints4HLS.Clear();
                }
                chart.EndUpdate();
            }
        }

        private void updateHLS5(SeriesPoint[] data)
        {
            data = data.Where(x => x.Y != -130).ToArray();

            if (data.Length > 0)
            {
                if (chart.ViewXY.HighLowSeries[1].Points == null || chart.ViewXY.HighLowSeries[1].Points.Count() == 0)
                {
                    int pointCounter = data.Length;
                    var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                    for (int i = 0; i < pointCounter; i++)
                    {
                        pointsHighLow[i].X = data[i].X;
                        pointsHighLow[i].YHigh = data[i].Y;
                        pointsHighLow[i].YLow = data[i].Y;
                    }

                    HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                    newLhlspoints4HLS.Add(temp);

                    chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                }
                else
                {
                    var lastnewListHLSPArray = newLhlspoints4HLS[newLhlspoints4HLS.Count - 1].highLowSeriesPoints;
                    if (lastnewListHLSPArray.Count() == data.Length &&
                        lastnewListHLSPArray[0].X == data[0].X && lastnewListHLSPArray[data.Length - 1].X == data[data.Length - 1].X)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i].Y > lastnewListHLSPArray[i].YHigh)
                            {
                                lastnewListHLSPArray[i].YHigh = data[i].Y;

                            }
                            if (lastnewListHLSPArray[i].YLow > data[i].Y)
                            {
                                lastnewListHLSPArray[i].YLow = data[i].Y;
                            }
                        }
                        newLhlspoints4HLS[newLhlspoints4HLS.Count() - 1] = new HighLowSeriesPointsPlusDateTime(lastnewListHLSPArray, DateTime.Now);
                    }
                    else
                    {
                        int pointCounter = data.Length;
                        var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                        for (int i = 0; i < pointCounter; i++)
                        {
                            pointsHighLow[i].X = data[i].X;
                            pointsHighLow[i].YHigh = data[i].Y;
                            pointsHighLow[i].YLow = data[i].Y;
                        }
                        HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                        newLhlspoints4HLS.Add(temp);
                    }

                    for (int i = 0; i < newLhlspoints4HLS.Count(); i++)
                    {
                        var diff = DateTime.Now - newLhlspoints4HLS[i].dateTime;
                        if (diff.TotalSeconds > _SavePlotStorageTime)
                            newLhlspoints4HLS.RemoveAt(i);
                    }

                    List<HighLowSeriesPoint> lpslpoints = new List<HighLowSeriesPoint>();

                    for (int w = 0; w < newLhlspoints4HLS.Count(); w++)
                    {
                        lpslpoints.AddRange(newLhlspoints4HLS[w].highLowSeriesPoints);
                    }

                    //chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                    chart.BeginUpdate();
                    try
                    {
                        chart.ViewXY.HighLowSeries[1].Points = lpslpoints.ToArray();
                    }
                    catch
                    {
                        chart.ViewXY.HighLowSeries[1].Clear();
                        newLhlspoints4HLS.Clear();
                    }
                    chart.EndUpdate();
                }
            }
        }

        private void updateHLS6(SeriesPoint[] data)
        {
            //data = data.Where(x => x.Y != -130).ToArray();

            if (data.Length > 0)
            {
                if (chart.ViewXY.HighLowSeries[1].Points == null || chart.ViewXY.HighLowSeries[1].Points.Count() == 0)
                {
                    int pointCounter = data.Length;
                    var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                    for (int i = 0; i < pointCounter; i++)
                    {
                        pointsHighLow[i].X = data[i].X;
                        pointsHighLow[i].YHigh = data[i].Y;
                        pointsHighLow[i].YLow = data[i].Y;
                    }

                    HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                    newLhlspoints4HLS.Add(temp);

                    chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                }
                else
                {
                    var lastnewListHLSPArray = newLhlspoints4HLS[newLhlspoints4HLS.Count - 1].highLowSeriesPoints;
                    if (lastnewListHLSPArray.Count() == data.Length &&
                        lastnewListHLSPArray[0].X == data[0].X && lastnewListHLSPArray[data.Length - 1].X == data[data.Length - 1].X)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i].Y > lastnewListHLSPArray[i].YHigh)
                            {
                                lastnewListHLSPArray[i].YHigh = data[i].Y;

                            }
                            if (lastnewListHLSPArray[i].YLow > data[i].Y && data[i].Y != -130)
                            {
                                lastnewListHLSPArray[i].YLow = data[i].Y;
                            }
                        }
                        newLhlspoints4HLS[newLhlspoints4HLS.Count() - 1] = new HighLowSeriesPointsPlusDateTime(lastnewListHLSPArray, DateTime.Now);
                    }
                    else
                    {
                        int pointCounter = data.Length;
                        var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                        for (int i = 0; i < pointCounter; i++)
                        {
                            pointsHighLow[i].X = data[i].X;
                            pointsHighLow[i].YHigh = data[i].Y;
                            pointsHighLow[i].YLow = data[i].Y;
                        }
                        HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                        newLhlspoints4HLS.Add(temp);
                    }

                    for (int i = 0; i < newLhlspoints4HLS.Count(); i++)
                    {
                        var diff = DateTime.Now - newLhlspoints4HLS[i].dateTime;
                        if (diff.TotalSeconds > _SavePlotStorageTime)
                            newLhlspoints4HLS.RemoveAt(i);
                    }

                    List<HighLowSeriesPoint> lpslpoints = new List<HighLowSeriesPoint>();

                    for (int w = 0; w < newLhlspoints4HLS.Count(); w++)
                    {
                        lpslpoints.AddRange(newLhlspoints4HLS[w].highLowSeriesPoints);
                    }

                    //chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                    chart.BeginUpdate();
                    try
                    {
                        chart.ViewXY.HighLowSeries[1].Points = lpslpoints.ToArray();
                    }
                    catch
                    {
                        chart.ViewXY.HighLowSeries[1].Clear();
                        newLhlspoints4HLS.Clear();
                    }
                    chart.EndUpdate();
                }
            }
        }

        private void updateHLS7(SeriesPoint[] data)
        {
            data = data.Where(x => x.Y != -130).ToArray();

            if (data.Length > 0)
            {
                if (chart.ViewXY.HighLowSeries[1].Points == null || chart.ViewXY.HighLowSeries[1].Points.Count() == 0)
                {
                    int pointCounter = data.Length;
                    var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                    for (int i = 0; i < pointCounter; i++)
                    {
                        pointsHighLow[i].X = data[i].X;
                        pointsHighLow[i].YHigh = data[i].Y;
                        pointsHighLow[i].YLow = data[i].Y;
                    }

                    HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                    newLhlspoints4HLS.Add(temp);

                    chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                }
                else
                {
                    var lastnewListHLSPArray = newLhlspoints4HLS[newLhlspoints4HLS.Count - 1].highLowSeriesPoints;
                    if (lastnewListHLSPArray.Count() == data.Length &&
                        lastnewListHLSPArray[0].X == data[0].X && lastnewListHLSPArray[data.Length - 1].X == data[data.Length - 1].X)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i].Y > lastnewListHLSPArray[i].YHigh)
                            {
                                lastnewListHLSPArray[i].YHigh = data[i].Y;
                                lastnewListHLSPArray[i].YLow = data[i].Y;
                            }
                        }
                        newLhlspoints4HLS[newLhlspoints4HLS.Count() - 1] = new HighLowSeriesPointsPlusDateTime(lastnewListHLSPArray, DateTime.Now);
                    }
                    else
                    {
                        int pointCounter = data.Length;
                        var pointsHighLow = new HighLowSeriesPoint[pointCounter];
                        for (int i = 0; i < pointCounter; i++)
                        {
                            pointsHighLow[i].X = data[i].X;
                            pointsHighLow[i].YHigh = data[i].Y;
                            pointsHighLow[i].YLow = data[i].Y;
                        }
                        HighLowSeriesPointsPlusDateTime temp = new HighLowSeriesPointsPlusDateTime(pointsHighLow, DateTime.Now);
                        newLhlspoints4HLS.Add(temp);
                    }

                    for (int i = 0; i < newLhlspoints4HLS.Count(); i++)
                    {
                        var diff = DateTime.Now - newLhlspoints4HLS[i].dateTime;
                        if (diff.TotalSeconds > _SavePlotStorageTime)
                            newLhlspoints4HLS.RemoveAt(i);
                    }

                    List<HighLowSeriesPoint> lpslpoints = new List<HighLowSeriesPoint>();

                    for (int w = 0; w < newLhlspoints4HLS.Count(); w++)
                    {
                        lpslpoints.AddRange(newLhlspoints4HLS[w].highLowSeriesPoints);
                    }

                    var keys = lpslpoints.Select(x => x.X).ToArray();
                    var items = lpslpoints.ToArray();
                    Array.Sort(keys, items);

                    //chart.ViewXY.HighLowSeries[1].Points = pointsHighLow;
                    chart.BeginUpdate();
                    try
                    {
                        chart.ViewXY.HighLowSeries[1].Points = items;
                    }
                    catch
                    {
                        chart.ViewXY.HighLowSeries[1].Clear();
                        newLhlspoints4HLS.Clear();
                    }
                    chart.EndUpdate();
                }
            }
        }

        public void ClearPersistentLayerV3()
        {
            if (chart.ViewXY.HighLowSeries[1] != null)
            {
                chart.ViewXY.HighLowSeries[1].Clear();
            }
            newLhlspoints4HLS.Clear();
        }

        private int SearchMinLeft(int k, double[] data, int Threshold, double prevmin)
        {
            int countmin = 0;

            while ((k - 1 > 0) && data[k] >= data[k - 1] || data[k] > Threshold)
            {
                k--;

                if (k - 1 < 0)
                    break;

                countmin++;

                if (data[k] <= prevmin)
                    break;
            }

            return countmin;
        }

        private int SearchMinRight(int k, double[] data, int Threshold, double prevmin)
        {
            int countmin = 0;

            for (; k < data.Count() - 1; k++)
            {
                if (data[k] >= data[k + 1] || data[k] > Threshold)
                    countmin++;
                if (data[k] <= prevmin)
                    break;
            }
            return countmin;
        }


        public void DrawRSArrows(Color[] color, double[] xData, double[] yData)
        {
            List<AnnotationXY> annotations = new List<AnnotationXY>();

            for (int i = 0; i < xData.Length; i++)
            {
                AnnotationXY tempAnnXY = new AnnotationXY();

                tempAnnXY.ArrowLineStyle.Color = color[i];
                tempAnnXY.TargetAxisValues.X = xData[i];
                tempAnnXY.TargetAxisValues.Y = yData[i];

                tempAnnXY.Style = AnnotationStyle.Arrow;
                tempAnnXY.ArrowStyleBegin = ArrowStyle.None;
                tempAnnXY.ArrowLineStyle.AntiAliasing = LineAntialias.Normal;
                tempAnnXY.ArrowEndAspectRatio = 1.5f;
                tempAnnXY.ArrowLineStyle.Width = 2;
                tempAnnXY.Text = "";
                tempAnnXY.MouseInteraction = false;
                tempAnnXY.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;

                annotations.Add(tempAnnXY);
            }

            chart.ViewXY.Annotations.AddRange(annotations);

        }

        public void DrawRSArrows(bool isActive, double[] xData, double[] yData)
        {
            List<AnnotationXY> annotations = new List<AnnotationXY>();

            for (int i = 0; i < xData.Length; i++)
            {
                AnnotationXY tempAnnXY = new AnnotationXY();

                tempAnnXY.ArrowLineStyle.Color = (isActive) ? Colors.Red : Colors.White;
                tempAnnXY.TargetAxisValues.X = xData[i];
                tempAnnXY.TargetAxisValues.Y = yData[i];

                tempAnnXY.Style = AnnotationStyle.Arrow;
                tempAnnXY.ArrowStyleBegin = ArrowStyle.None;
                tempAnnXY.ArrowLineStyle.AntiAliasing = LineAntialias.Normal;
                tempAnnXY.ArrowEndAspectRatio = 1.5f;
                tempAnnXY.ArrowLineStyle.Width = 2;
                tempAnnXY.Text = "";
                tempAnnXY.MouseInteraction = false;
                tempAnnXY.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;

                annotations.Add(tempAnnXY);
            }

            chart.ViewXY.Annotations.AddRange(annotations);

        }

        public void DrawRSLines(double[] xData, double yLevel)
        {
            List<AnnotationXY> annotations = new List<AnnotationXY>();

            for (int i = 0; i < xData.Length; i++)
            {
                AnnotationXY tempAnnXY = new AnnotationXY();

                tempAnnXY.ArrowLineStyle.Color = Colors.Red;
                tempAnnXY.TargetAxisValues.X = xData[i];
                tempAnnXY.TargetAxisValues.Y = yLevel;

                tempAnnXY.Style = AnnotationStyle.Arrow;
                tempAnnXY.ArrowStyleBegin = ArrowStyle.None;
                tempAnnXY.ArrowStyleEnd = ArrowStyle.None;
                tempAnnXY.ArrowLineStyle.AntiAliasing = LineAntialias.Normal;
                tempAnnXY.ArrowLineStyle.Width = 1;
                tempAnnXY.Text = "";
                tempAnnXY.MouseInteraction = false;
                tempAnnXY.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;

                annotations.Add(tempAnnXY);
            }

            chart.ViewXY.Annotations.AddRange(annotations);
        }

        public void DrawRSLines2(double[] xData, double yLevel = -80)
        {
            List<AnnotationXY> annotations = new List<AnnotationXY>();

            for (int i = 0; i < xData.Length; i++)
            {
                for (int w = 0; w < lSeriesPoints?.Count() - 1; w = w + 2)
                {
                    if (xData[i] >= lSeriesPoints[w].X && xData[i] <= lSeriesPoints[w + 1].X)
                    {
                        AnnotationXY tempAnnXY = new AnnotationXY();

                        tempAnnXY.TargetAxisValues.X = xData[i];
                        tempAnnXY.TargetAxisValues.Y = lSeriesPoints[w].Y;
                        tempAnnXY.ArrowLineStyle.Color = lSeriesPoints[w].PointColor;

                        tempAnnXY.Style = AnnotationStyle.Arrow;
                        tempAnnXY.ArrowStyleBegin = ArrowStyle.None;
                        tempAnnXY.ArrowStyleEnd = ArrowStyle.None;
                        tempAnnXY.ArrowLineStyle.AntiAliasing = LineAntialias.Normal;
                        tempAnnXY.ArrowLineStyle.Width = 1;
                        tempAnnXY.Text = "";
                        tempAnnXY.MouseInteraction = false;
                        tempAnnXY.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;

                        annotations.Add(tempAnnXY);
                    }
                }
            }
            chart.ViewXY.Annotations.AddRange(annotations);
        }


        public void DrawRSLines(List<double[]> lxData, List<double> lyLevel)
        {
            List<AnnotationXY> annotations = new List<AnnotationXY>();

            for (int w = 0; w < lxData.Count(); w++)
            {
                for (int i = 0; i < lxData[w].Length; i++)
                {
                    AnnotationXY tempAnnXY = new AnnotationXY();

                    tempAnnXY.ArrowLineStyle.Color = AuroraBorealColorPalette(w);
                    tempAnnXY.TargetAxisValues.X = lxData[w][i];
                    tempAnnXY.TargetAxisValues.Y = lyLevel[w];

                    tempAnnXY.Style = AnnotationStyle.Arrow;
                    tempAnnXY.ArrowStyleBegin = ArrowStyle.None;
                    tempAnnXY.ArrowStyleEnd = ArrowStyle.None;
                    tempAnnXY.ArrowLineStyle.AntiAliasing = LineAntialias.Normal;
                    tempAnnXY.ArrowLineStyle.Width = 1;
                    tempAnnXY.Text = "";
                    tempAnnXY.MouseInteraction = false;
                    tempAnnXY.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;

                    annotations.Add(tempAnnXY);
                }
            }
            chart.ViewXY.Annotations.AddRange(annotations);
        }

        public Color AuroraBorealColorPalette(int colorNumber)
        {
            switch (colorNumber)
            {
                case 0:
                    return Color.FromRgb(255, 0, 0);
                case 1:
                    return Color.FromRgb(255, 165, 0);
                case 2:
                    return Color.FromRgb(255, 69, 0);
                case 3:
                    return Color.FromRgb(255, 255, 0);
                default:
                    return Color.FromRgb(255, 0, 0);
            }
        }

        public void ClearArrowsAndLines()
        {
            chart.ViewXY.Annotations.Clear();
        }

        public void ClearArrowsAndLines2()
        {
            chart.ViewXY.Annotations.Clear();
            //lSeriesPoints.Clear();
        }

        private bool _FHSSonRSVisible = false;
        public bool FHSSonRSVisible
        {
            get { return _FHSSonRSVisible; }
            set
            {
                if (_FHSSonRSVisible != value)
                {
                    _FHSSonRSVisible = value;

                    //int N = chart.ViewXY.PointLineSeries.Count();
                    for (int i = 2; i < 2 + FHSSRangesCount; i++)
                    {
                        DispatchIfNecessary(() =>
                        {
                            chart.ViewXY.PointLineSeries[i].Visible = _FHSSonRSVisible;
                        });
                    }
                }
            }
        }


        private struct TwoDouble
        {
            public double start;
            public double end;

            public TwoDouble(double start, double end)
            {
                this.start = start;
                this.end = end;
            }
        }

        private TwoDouble[] ArrSE;
        private List<TwoDouble>[] ArrLTW;

        public void FHSSonRS(List<double> lFreqStart, List<double> lFreqEnd, List<double[]> lCutOffFreq, List<double[]> lCutOffWidth)
        {
            ArrSE = new TwoDouble[lFreqStart.Count()];
            ArrLTW = new List<TwoDouble>[lFreqStart.Count()];

            for (int i = 0; i < lFreqStart.Count(); i++)
            {
                ArrSE[i].start = lFreqStart[i];
                ArrSE[i].end = lFreqEnd[i];
                ArrLTW[i] = new List<TwoDouble>();

                SortedDictionary<double, double> SDii = new SortedDictionary<double, double>();
                for (int j = 0; j < lCutOffFreq[i].Count(); j++)
                {
                    SDii.Add(lCutOffFreq[i][j], lCutOffWidth[i][j]);
                }
                for (int j = 0; j < lCutOffFreq[i].Count(); j++)
                {
                    //double start = (SDii.ElementAt(j).Key - (SDii.ElementAt(j).Value / 2)) / 10000d;
                    //double end = (SDii.ElementAt(j).Key + (SDii.ElementAt(j).Value / 2)) / 10000d;
                    double start = (SDii.ElementAt(j).Key - (SDii.ElementAt(j).Value / 2));
                    double end = (SDii.ElementAt(j).Key + (SDii.ElementAt(j).Value / 2));
                    ArrLTW[i].Add(new TwoDouble(start, end));
                }
            }

            //Переработка на наличие ненужных выколотых частот
            for (int i = 0; i < ArrLTW.Length; i++)
            {
                for (int j = 0; j < ArrLTW[i].Count(); j++)
                {
                    //1+2
                    if (j != -1)
                        if (ArrLTW[i][j].start < ArrSE[i].start && ArrLTW[i][j].end <= ArrSE[i].start)
                        {
                            ArrLTW[i].RemoveAt(j);
                            j--;
                        }

                    //3
                    if (j != -1)
                        if (ArrLTW[i][j].start <= ArrSE[i].start && (ArrLTW[i][j].end > ArrSE[i].start && ArrLTW[i][j].end < ArrSE[i].end))
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrSE[i].start, ArrLTW[i][j].end);
                        }

                    //4+5+6 - Всё ок

                    //7
                    if (j != -1)
                        if ((ArrLTW[i][j].start > ArrSE[i].start && ArrLTW[i][j].start < ArrSE[i].end) && ArrLTW[i][j].end >= ArrSE[i].end)
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrLTW[i][j].start, ArrSE[i].end);
                        }

                    //8+9
                    if (j != -1)
                        if (ArrLTW[i][j].start >= ArrSE[i].end && ArrLTW[i][j].end > ArrSE[i].end)
                        {
                            ArrLTW[i].RemoveAt(j);
                            j--;
                        }

                    //10
                    if (j != -1)
                        if (ArrLTW[i][j].start < ArrSE[i].start && ArrLTW[i][j].end > ArrSE[i].end)
                        {
                            var temp = ArrSE.ToList();
                            temp.RemoveAt(i);
                            ArrSE = temp.ToArray();
                        }
                }
            }


            //Переработка на наличие пересечений
            for (int i = 0; i < ArrLTW.Length; i++)
            {
                for (int j = 0; j < ArrLTW[i].Count(); j++)
                {
                    if (j + 1 < ArrLTW[i].Count())
                        if (ArrLTW[i][j].end >= ArrLTW[i][j + 1].start)
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrLTW[i][j].start, ArrLTW[i][j + 1].end);
                            ArrLTW[i].RemoveAt(j + 1);
                            j--;
                        }
                }
            }


            for (int i = 0; i < ArrSE.Count(); i++)
            {
                PointLineSeries PLS = new PointLineSeries();
                PLS.MouseInteraction = false;
                PLS.LineStyle.Color = Colors.White;
                PLS.LineStyle.Width = 2;

                PLS.Visible = _FHSSonRSVisible;

                PLS.Points = RecPointsToFHSS(ArrSE[i], i);

                chart.ViewXY.PointLineSeries.Add(PLS);
            }

        }
        public void FHSSonRS(List<double> lFreqStart, List<double> lFreqEnd, List<double[]> lCutOffFreq, List<double[]> lCutOffWidth, List<double> lThreshold)
        {
            ArrSE = new TwoDouble[lFreqStart.Count()];
            ArrLTW = new List<TwoDouble>[lFreqStart.Count()];

            for (int i = 0; i < lFreqStart.Count(); i++)
            {
                ArrSE[i].start = lFreqStart[i];
                ArrSE[i].end = lFreqEnd[i];
                ArrLTW[i] = new List<TwoDouble>();

                SortedDictionary<double, double> SDii = new SortedDictionary<double, double>();
                for (int j = 0; j < lCutOffFreq[i].Count(); j++)
                {
                    SDii.Add(lCutOffFreq[i][j], lCutOffWidth[i][j]);
                }
                for (int j = 0; j < lCutOffFreq[i].Count(); j++)
                {
                    //double start = (SDii.ElementAt(j).Key - (SDii.ElementAt(j).Value / 2)) / 10000d;
                    //double end = (SDii.ElementAt(j).Key + (SDii.ElementAt(j).Value / 2)) / 10000d;
                    double start = (SDii.ElementAt(j).Key - (SDii.ElementAt(j).Value / 2));
                    double end = (SDii.ElementAt(j).Key + (SDii.ElementAt(j).Value / 2));
                    ArrLTW[i].Add(new TwoDouble(start, end));
                }
            }

            //Переработка на наличие ненужных выколотых частот
            for (int i = 0; i < ArrLTW.Length; i++)
            {
                for (int j = 0; j < ArrLTW[i].Count(); j++)
                {
                    //1+2
                    if (j != -1)
                        if (ArrLTW[i][j].start < ArrSE[i].start && ArrLTW[i][j].end <= ArrSE[i].start)
                        {
                            ArrLTW[i].RemoveAt(j);
                            j--;
                        }

                    //3
                    if (j != -1)
                        if (ArrLTW[i][j].start <= ArrSE[i].start && (ArrLTW[i][j].end > ArrSE[i].start && ArrLTW[i][j].end < ArrSE[i].end))
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrSE[i].start, ArrLTW[i][j].end);
                        }

                    //4+5+6 - Всё ок

                    //7
                    if (j != -1)
                        if ((ArrLTW[i][j].start > ArrSE[i].start && ArrLTW[i][j].start < ArrSE[i].end) && ArrLTW[i][j].end >= ArrSE[i].end)
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrLTW[i][j].start, ArrSE[i].end);
                        }

                    //8+9
                    if (j != -1)
                        if (ArrLTW[i][j].start >= ArrSE[i].end && ArrLTW[i][j].end > ArrSE[i].end)
                        {
                            ArrLTW[i].RemoveAt(j);
                            j--;
                        }

                    //10
                    if (j != -1)
                        if (ArrLTW[i][j].start < ArrSE[i].start && ArrLTW[i][j].end > ArrSE[i].end)
                        {
                            var temp = ArrSE.ToList();
                            temp.RemoveAt(i);
                            ArrSE = temp.ToArray();
                        }
                }
            }


            //Переработка на наличие пересечений
            for (int i = 0; i < ArrLTW.Length; i++)
            {
                for (int j = 0; j < ArrLTW[i].Count(); j++)
                {
                    if (j + 1 < ArrLTW[i].Count())
                        if (ArrLTW[i][j].end >= ArrLTW[i][j + 1].start)
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrLTW[i][j].start, ArrLTW[i][j + 1].end);
                            ArrLTW[i].RemoveAt(j + 1);
                            j--;
                        }
                }
            }

            FHSSRangesCount = ArrSE.Count();
            for (int i = 0; i < ArrSE.Count(); i++)
            {
                PointLineSeries PLS = new PointLineSeries();
                PLS.MouseInteraction = false;
                PLS.LineStyle.Color = Colors.White;
                PLS.LineStyle.Width = 2;

                PLS.Visible = _FHSSonRSVisible;

                PLS.Points = RecPointsToFHSS(ArrSE[i], i, lThreshold[i]);
                ExtractSeriesPoints(PLS.Points, i);
                //chart.ViewXY.PointLineSeries.Add(PLS);
                chart.ViewXY.PointLineSeries.Insert(2 + i, PLS);
            }

        }

        List<SeriesPoint> lSeriesPoints = new List<SeriesPoint>();

        private void ExtractSeriesPoints(SeriesPoint[] seriesPoints, int w)
        {
            if (seriesPoints?.Count() >= 4)
            {
                for (int i = 1; i < seriesPoints.Count(); i = i + 4)
                {
                    lSeriesPoints.Add(new SeriesPoint(seriesPoints[i].X, seriesPoints[i].Y, AuroraBorealColorPalette(w)));
                    lSeriesPoints.Add(new SeriesPoint(seriesPoints[i + 1].X, seriesPoints[i + 1].Y, AuroraBorealColorPalette(w)));
                }
            }
        }

        private SeriesPoint[] RecPointsToFHSS(TwoDouble ArrSE, int index)
        {
            List<SeriesPoint> lSP = new List<SeriesPoint>();
            if (ArrLTW[index].Count == 0)
            {
                var fourPoints = Calc4Points(ArrSE.start, ArrSE.end, -80.0, _GlobalRangeYmin);
                lSP.AddRange(fourPoints);
            }
            else
            {
                if (ArrLTW[index].Count == 1)
                {
                    var fourPoints = Calc4Points(ArrSE.start, ArrLTW[index][0].start, -80.0, _GlobalRangeYmin);
                    lSP.AddRange(fourPoints);

                    fourPoints = Calc4Points(ArrLTW[index][0].end, ArrSE.end, -80.0, _GlobalRangeYmin);
                    lSP.AddRange(fourPoints);
                }
                else
                {
                    for (int j = 0; j < ArrLTW[index].Count; j++)
                    {
                        if (j == 0)
                        {
                            var fourPoints = Calc4Points(ArrSE.start, ArrLTW[index][j].start, -80.0, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);

                        }
                        if ((0 < j) && (j < ArrLTW[index].Count - 1))
                        {
                            var fourPoints = Calc4Points(ArrLTW[index][j - 1].end, ArrLTW[index][j].start, -80.0, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);

                        }
                        if (j == ArrLTW[index].Count - 1)
                        {
                            var fourPoints = Calc4Points(ArrLTW[index][j - 1].end, ArrLTW[index][j].start, -80.0, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);

                            fourPoints = Calc4Points(ArrLTW[index][j].end, ArrSE.end, -80.0, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);
                        }
                    }
                }
            }
            return lSP.ToArray();
        }
        private SeriesPoint[] RecPointsToFHSS(TwoDouble ArrSE, int index, double threshold)
        {
            List<SeriesPoint> lSP = new List<SeriesPoint>();
            if (ArrLTW[index].Count == 0)
            {
                var fourPoints = Calc4Points(ArrSE.start, ArrSE.end, threshold, _GlobalRangeYmin);
                lSP.AddRange(fourPoints);
            }
            else
            {
                if (ArrLTW[index].Count == 1)
                {
                    var fourPoints = Calc4Points(ArrSE.start, ArrLTW[index][0].start, threshold, _GlobalRangeYmin);
                    lSP.AddRange(fourPoints);

                    fourPoints = Calc4Points(ArrLTW[index][0].end, ArrSE.end, threshold, _GlobalRangeYmin);
                    lSP.AddRange(fourPoints);
                }
                else
                {
                    for (int j = 0; j < ArrLTW[index].Count; j++)
                    {
                        if (j == 0)
                        {
                            var fourPoints = Calc4Points(ArrSE.start, ArrLTW[index][j].start, threshold, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);

                        }
                        if ((0 < j) && (j < ArrLTW[index].Count - 1))
                        {
                            var fourPoints = Calc4Points(ArrLTW[index][j - 1].end, ArrLTW[index][j].start, threshold, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);

                        }
                        if (j == ArrLTW[index].Count - 1)
                        {
                            var fourPoints = Calc4Points(ArrLTW[index][j - 1].end, ArrLTW[index][j].start, threshold, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);

                            fourPoints = Calc4Points(ArrLTW[index][j].end, ArrSE.end, threshold, _GlobalRangeYmin);
                            lSP.AddRange(fourPoints);
                        }
                    }
                }
            }
            return lSP.ToArray();
        }

        private SeriesPoint[] Calc4Points(double freqStart, double freqEnd, double maxLevel, double minLevel)
        {
            var points = new SeriesPoint[4];

            points[0].X = freqStart;
            points[0].Y = minLevel;
            points[1].X = freqStart;
            points[1].Y = maxLevel;
            points[2].X = freqEnd;
            points[2].Y = maxLevel;
            points[3].X = freqEnd;
            points[3].Y = minLevel;

            return points;
        }

        private int FHSSRangesCount = 0;

        public void ClearFHSSonRS()
        {
            //if (chart.ViewXY.PointLineSeries.Count() > 1)
            if (chart.ViewXY.PointLineSeries.Count() > 2)
            {
                //chart.ViewXY.PointLineSeries.RemoveRange(1, chart.ViewXY.PointLineSeries.Count() - 1);
                //chart.ViewXY.PointLineSeries.RemoveRange(2, chart.ViewXY.PointLineSeries.Count() - 1);
                //chart.ViewXY.PointLineSeries.RemoveRange(2, FHSSRangesCount);
                for (int i = 0; i < FHSSRangesCount; i++)
                {
                    chart.ViewXY.PointLineSeries.RemoveAt(2);
                }
                FHSSRangesCount = 0;
            }
        }


        private double[] GenerateDouble()
        {
            double s = _GlobalRangeXmin;
            double w = _GlobalBandWidthMHz;
            int n = _GlobalNumberOfBands;
            double[] b = new double[n + 1];
            for (int i = 0; i < n + 1; i++)
            {
                b[i] = s + w * i;
            }
            return b;
        }

        private PointLineSeries GeneratePLS(SeriesPoint[] stairLinePoints)
        {
            // The new series using the second y-axis.
            PointLineSeries lsSegment = new PointLineSeries(chart.ViewXY, chart.ViewXY.XAxes[0], chart.ViewXY.YAxes[0]);
            lsSegment.MouseInteraction = false;
            lsSegment.Title.Visible = false;
            lsSegment.LineStyle.Color = Colors.Gray;
            lsSegment.Title.Color = lsSegment.LineStyle.Color;
            lsSegment.LineStyle.Width = 2f;
            lsSegment.LineStyle.Pattern = LinePattern.Dot;
            lsSegment.LineStyle.AntiAliasing = LineAntialias.Normal;
            lsSegment.MouseHighlight = MouseOverHighlight.Simple;
            lsSegment.Points = stairLinePoints;
            return lsSegment;
        }

        private List<SeriesPoint> GenerateLSP(double[] GlobalBands, int[] Bands, int[] iThresholds)
        {
            List<SeriesPoint> LSP = new List<SeriesPoint>();
            for (int i = 0; i < Bands.Count(); i++)
            {
                SeriesPoint srcPoint1 = new SeriesPoint();
                srcPoint1.X = GlobalBands[Bands[i]];
                srcPoint1.Y = iThresholds[i];
                LSP.Add(srcPoint1);
                SeriesPoint srcPoint2 = new SeriesPoint();
                srcPoint2.X = GlobalBands[Bands[i] + 1];
                srcPoint2.Y = iThresholds[i];
                LSP.Add(srcPoint2);
            }
            return LSP;
        }

        private List<Dictionary<int, int>> ExtractSegments(byte[] Bands, byte[] Thresholds)
        {
            List<Dictionary<int, int>> LofS = new List<Dictionary<int, int>>();
            Dictionary<int, int> temp = new Dictionary<int, int>();

            for (int i = 0; i < Bands.Count(); i++)
            {
                if (i + 1 < Bands.Count())
                {
                    if (Bands[i + 1] - Bands[i] == 1)
                    {
                        temp.Add(Bands[i], (-1) * Thresholds[i]);
                    }
                    else
                    {
                        temp.Add(Bands[i], (-1) * Thresholds[i]);
                        LofS.Add(new Dictionary<int, int>(temp));
                        temp.Clear();
                    }
                }
                else
                {
                    temp.Add(Bands[i], (-1) * Thresholds[i]);
                    if (Bands[Bands.Count() - 1] - temp.ElementAt(temp.Count - 1).Key == 1)
                    {
                        temp.Add(Bands[Bands.Count() - 1], (-1) * Thresholds[Bands.Count() - 1]);
                        LofS.Add(new Dictionary<int, int>(temp));
                        temp.Clear();
                    }
                    else
                    {
                        LofS.Add(new Dictionary<int, int>(temp));
                        temp.Clear();
                    }
                }
            }

            return LofS;
        }

        public void AdaptiveBandsPaint(byte[] Bands, byte[] Thresholds)
        {
            if (chart.ViewXY.PointLineSeries.Count > 2 + FHSSRangesCount)
            {
                chart.ViewXY.PointLineSeries.RemoveRange(2 + FHSSRangesCount, chart.ViewXY.PointLineSeries.Count - 1);
            }

            List<Dictionary<int, int>> LofS = ExtractSegments(Bands, Thresholds);

            var GlobalBands = GenerateDouble();

            for (int w = 0; w < LofS.Count(); w++)
            {
                List<SeriesPoint> LSP = GenerateLSP(GlobalBands, LofS[w].Keys.ToArray(), LofS[w].Values.ToArray());

                var sp = MakeStairLinePoints(LSP.ToArray());

                var lsSegment = GeneratePLS(sp);

                chart.ViewXY.PointLineSeries.Add(lsSegment);

                LSP.Clear();
            }
        }

        private SeriesPoint[] MakeStairLinePoints(SeriesPoint[] regularPoints)
        {
            int valuesCount = regularPoints.Length;
            int stairValueCount = 2 * valuesCount - 1;
            SeriesPoint[] stairPoints = new SeriesPoint[stairValueCount];
            int targetIndex = 0;

            for (int i = 0; i < valuesCount; i++)
            {
                if (i > 0)
                {
                    stairPoints[targetIndex].X = regularPoints[i].X;
                    stairPoints[targetIndex++].Y = regularPoints[i - 1].Y;
                }

                stairPoints[targetIndex].X = regularPoints[i].X;
                stairPoints[targetIndex++].Y = regularPoints[i].Y;
            }

            return stairPoints;
        }

        public void HideSpectrumAndAdaptive(bool value)
        {
            chart.ViewXY.PointLineSeries[0].Visible = value;
            chart.ViewXY.PointLineSeries[1].Visible = value;

            for (int i = 2 + FHSSRangesCount; i < chart.ViewXY.PointLineSeries.Count; i++)
            {
                chart.ViewXY.PointLineSeries[i].Visible = value;
            }
        }

        #endregion

        #region Private Metods

        private void SetOriginScale()
        {
            xAxis.SetRange(_GlobalRangeXmin, _GlobalRangeXmax);
            yAxis.SetRange(_GlobalRangeYmin, _GlobalRangeYmax);
        }

        private void ClearSpectr()
        {
            pointSeries.Points = new SeriesPoint[0];
        }

        private void outBandPictureBoxIndex(double MinFrequency, double MaxFrequency, out int startIndex, out int endIndex)
        {
            startIndex = 0;
            endIndex = _GlobalNumberOfBands;

            if (MinFrequency < _GlobalRangeXmin)
                MinFrequency = _GlobalRangeXmin;

            if (MaxFrequency > _GlobalRangeXmax)
                MaxFrequency = _GlobalRangeXmax;

            double[] td = new double[_GlobalNumberOfBands];
            for (int i = 0; i < _GlobalNumberOfBands; i++)
            {
                td[i] = _GlobalRangeXmin + i * GlobalBandWidthMHz;
                if (MinFrequency >= td[i] && MinFrequency <= td[i] + GlobalBandWidthMHz)
                    startIndex = i;
                if (MaxFrequency >= td[i] && MaxFrequency <= td[i] + GlobalBandWidthMHz)
                {
                    endIndex = i;
                    break;
                }
            }
        }

        private void outRangeFromIndex(out double startValue, out double endValue, int index)
        {
            startValue = _GlobalRangeXmin + _GlobalBandWidthMHz * index;
            endValue = startValue + _GlobalBandWidthMHz;
        }

        private void outRangeFromIndex(out double startValue, out double endValue, int startIndex, int endIndex)
        {
            startValue = _GlobalRangeXmin + _GlobalBandWidthMHz * startIndex;
            endValue = _GlobalRangeXmin + _GlobalBandWidthMHz * (endIndex + 1);
        }

        private void GenerateCustomTicks(ref AxisX axisX)
        {
            double[] intervals = new double[] { 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1 };

            double interval = axisX.Maximum - axisX.Minimum;
            double intervaldiv = interval / 6;

            var abs = intervals.Select(x => Math.Abs(x - intervaldiv)).ToArray();
            double minvalue = abs.Min();
            int indexmin = Array.IndexOf(abs, minvalue);

            int start = (int)(axisX.Minimum / intervals[indexmin]);

            List<double> dLTickValues = new List<double>();
            dLTickValues.Add(axisX.Minimum);

            var firststep = start * intervals[indexmin];
            while (firststep + intervals[indexmin] < axisX.Maximum)
            {
                firststep = firststep + intervals[indexmin];
                if (Math.Abs(axisX.Minimum - firststep) >= intervals[indexmin] * 0.95)
                    dLTickValues.Add(firststep);
            }
            if (Math.Abs(axisX.Maximum - dLTickValues[dLTickValues.Count() - 1]) >= intervals[indexmin])
                dLTickValues.Add(axisX.Maximum);
            else
                dLTickValues[dLTickValues.Count() - 1] = axisX.Maximum;

            //Set custom ticks for X axis,
            int[] aTickValues = new int[] { 25, 500, 1000, 1500, 2000, 2500, 3025 };

            var dTickValues = dLTickValues.ToArray();

            axisX.CustomTicks.Clear();
            for (int i = 0; i < dTickValues.Length; i++)
            {

                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    dTickValues[i],
                    dTickValues[i].ToString("G5"),
                    6,
                    true,
                    axisX.MajorDivTickStyle.Color,
                    CustomTickStyle.TickAndGrid);

                axisX.CustomTicks.Add(cTick);

            }

            double width = intervals[indexmin];
            double minorwidth = width / 5d;

            List<double> lMinors = new List<double>();

            double first = dTickValues[1];
            double almfirst = dTickValues[0];
            while (almfirst <= first - minorwidth)
            {
                lMinors.Add(first - minorwidth);
                first = first - minorwidth;
            }

            for (int i = 1; i < dTickValues.Count() - 1; i++)
            {
                lMinors.Add(dTickValues[i] + 1 * minorwidth);
                lMinors.Add(dTickValues[i] + 2 * minorwidth);
                lMinors.Add(dTickValues[i] + 3 * minorwidth);
                lMinors.Add(dTickValues[i] + 4 * minorwidth);
            }

            double last = dTickValues[dTickValues.Count() - 1];
            double almlast = dTickValues[dTickValues.Count() - 2];
            while (almlast + minorwidth <= last)
            {
                lMinors.Add(almlast + minorwidth);
                almlast = almlast + minorwidth;
            }

            var minors = lMinors.ToArray();
            Array.Sort(minors);

            for (int i = 0; i < minors.Length; i++)
            {
                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    minors[i],
                    "",
                    3,
                    true,
                    axisX.MinorDivTickStyle.Color,
                    CustomTickStyle.Tick);

                axisX.CustomTicks.Add(cTick);
            }

            //Allow showing the custom tick strings

            axisX.CustomTicksEnabled = true;

            axisX.InvalidateCustomTicks();
        }


        public event SimpleDoubleEventHandler nBarRangeMin;
        public event SimpleDoubleEventHandler nBarBandWidthMHz;
        public event SimpleIntEventHandler nBarNumberOfBands;

        private void _ReCalc()
        {
            _GlobalRangeXmax = _GlobalRangeXmin + _GlobalNumberOfBands * _GlobalBandWidthMHz;

            nBarRangeMin?.Invoke(_GlobalRangeXmin);
            nBarBandWidthMHz?.Invoke(_GlobalBandWidthMHz);
            nBarNumberOfBands?.Invoke(_GlobalNumberOfBands);

            //nBar.RangeMin = _GlobalRangeXmin;
            //nBar.BandWidthMHz = _GlobalBandWidthMHz;
            //nBar.NumberOfBands = _GlobalNumberOfBands;

            SetXRange(_GlobalRangeXmin, _GlobalRangeXmax);
        }

        private void SetXRange(double start, double end)
        {
            chart.BeginUpdate();
            xAxis.SetRange(start, end);
            chart.EndUpdate();
        }

        private void SetYRange(double start, double end)
        {
            yAxis.SetRange(start, end);
        }

        #endregion

        #region Handlers

        private void originalScale_Click(object sender, RoutedEventArgs e)
        {
            SetOriginScale();
        }
        private void moveLeft_Click(object sender, RoutedEventArgs e)
        {
            double interval = xAxis.Maximum - xAxis.Minimum;
            xAxis.SetRange(xAxis.Minimum - interval, xAxis.Maximum - interval);
        }
        private void moveRight_Click(object sender, RoutedEventArgs e)
        {
            double interval = xAxis.Maximum - xAxis.Minimum;
            xAxis.SetRange(xAxis.Minimum + interval, xAxis.Maximum + interval);
        }
        private void scaleIn_Click(object sender, RoutedEventArgs e)
        {
            double range = xAxis.Maximum - xAxis.Minimum;
            range = range / 8;
            xAxis.SetRange(xAxis.Minimum + range, xAxis.Maximum - range);
        }
        private void scaleOut_Click(object sender, RoutedEventArgs e)
        {
            double range = xAxis.Maximum - xAxis.Minimum;
            range = range / 4;
            xAxis.SetRange(xAxis.Minimum - range, xAxis.Maximum + range);
        }
        private void clear_Click(object sender, RoutedEventArgs e)
        {
            //ClearSpectr();
            //ClearStorage();
            ClearPersistentLayerV3();
        }
        public event SimpleBoolEventHandler StorageChange;
        public void accumulationOff()
        {
            //storage.IsChecked = false;
            saveStorage.IsChecked = false;
        }
        private void Storage_Checked(object sender, RoutedEventArgs e)
        {
            //StorageChange?.Invoke(storage.IsChecked.Value);
        }
        private void Storage_Unchecked(object sender, RoutedEventArgs e)
        {
            //StorageChange?.Invoke(storage.IsChecked.Value);
        }
        public event SimpleBoolEventHandler SaveStorageChange;
        private void SaveStorage_Checked(object sender, RoutedEventArgs e)
        {
            SaveStorageChange?.Invoke(saveStorage.IsChecked.Value);
        }

        private void SaveStorage_Unchecked(object sender, RoutedEventArgs e)
        {
            SaveStorageChange?.Invoke(saveStorage.IsChecked.Value);
        }
        public event SimpleBoolEventHandler PolarChange;
        private void polar_Click(object sender, RoutedEventArgs e)
        {
            PolarChange?.Invoke(polar.IsChecked.Value);
        }
        public void PolarToggleButton(bool value)
        {
            polar.IsChecked = value;
        }
        private void thresUp_Click(object sender, RoutedEventArgs e)
        {
            _Threshold++;
            RRLine.Value++;
        }
        private void thresDown_Click(object sender, RoutedEventArgs e)
        {
            _Threshold--;
            RRLine.Value--;
        }
        private void target_Click(object sender, RoutedEventArgs e)
        {
            if (CursorX.Visible == true && CursorY.Visible == true)
                FreqOnTargetEvent?.Invoke(cursorX, (short)cursorY);
        }
        private void radioSuppr_Click(object sender, RoutedEventArgs e)
        {
            if (CursorX.Visible == true && CursorY.Visible == true)
                FreqOnRSEvent?.Invoke(cursorX, (short)cursorY);
        }
        private void radioSupprFHSS_Click(object sender, RoutedEventArgs e)
        {
            if (band.Visible == true)
                BandOnRSEvent?.Invoke(band.ValueBegin, band.ValueEnd);
        }
        private void control_Click(object sender, RoutedEventArgs e)
        {
            if (CursorX.Visible == true && CursorY.Visible == true)
                FreqOnRSSEvent?.Invoke(cursorX, (short)cursorY, CRR.CRRX);
        }
        private void control_Click2(object sender, RoutedEventArgs e)
        {
            if (CursorX.Visible == true && CursorY.Visible == true)
                FreqOnRSSEvent?.Invoke(cursorX, (short)cursorY, CRR.CRRX2);
        }
        private void bearing_Click(object sender, RoutedEventArgs e)
        {
            if (band.Visible == true)
                AreaFreqOnBearing?.Invoke(band.ValueBegin, band.ValueEnd);
        }
        private void qbearing_Click(object sender, RoutedEventArgs e)
        {
            if (band.Visible == true)
                AreaFreqOnQBearing?.Invoke(band.ValueBegin, band.ValueEnd);
        }

        public void ExBearingOff()
        {
            exbearing.IsChecked = false;
        }

        public void StorageadndBearingEnabled(bool value)
        {
            exbearing.IsEnabled = value;
            //storage.IsEnabled = value;
            saveStorage.IsEnabled = value;

            qbearing.IsEnabled = value;
        }

        public void qBearingButtonEnabled(bool value)
        {
            qbearing.IsEnabled = value;
        }

        public void ExternalExBearing(double freqMHz, double freqWidthMHz)
        {
            if (exbearing.IsEnabled == true && exbearing.IsChecked == false)
            {
                CursorX.Visible = false;
                CursorY.Visible = false;

                band.ValueBegin = freqMHz - freqWidthMHz;
                band.ValueEnd = freqMHz + freqWidthMHz;

                band.Visible = true;

                exbearing.IsChecked = true;
            }
        }

        public delegate void SimpleBoolEventHandler(bool value);
        public event SimpleBoolEventHandler ExBearingChange;

        private void bearing_Cheked(object sender, RoutedEventArgs e)
        {
            if (!(band.ValueBegin < _GlobalRangeXmin || band.ValueBegin > _GlobalRangeXmax || band.ValueEnd < _GlobalRangeXmin || band.ValueEnd > _GlobalRangeXmax) && band.Visible == true)
            {
                AreaFreqOnBearing?.Invoke(band.ValueBegin, band.ValueEnd);
                ExBearingChange?.Invoke(exbearing.IsChecked.Value);
            }
            else
                ExBearingOff();
        }

        private void bearing_Uncheked(object sender, RoutedEventArgs e)
        {
            if (!(band.ValueBegin < _GlobalRangeXmin || band.ValueBegin > _GlobalRangeXmax || band.ValueEnd < _GlobalRangeXmin || band.ValueEnd > _GlobalRangeXmax))
                ExBearingChange?.Invoke(exbearing.IsChecked.Value);
        }

        private void setForb_Click(object sender, RoutedEventArgs e)
        {
            if (band.Visible == true)
            {
                //AreaFreqOnForbidden?.Invoke(band.ValueBegin, band.ValueEnd);
                OnFreqArea?.Invoke(FrequencyType.Forbidden, band.ValueBegin, band.ValueEnd);
            }
            else
            {
                if (CursorX.Visible == true && CursorY.Visible == true)
                    OnFreqArea?.Invoke(FrequencyType.Forbidden, cursorX, cursorX);
            }
        }

        private void setKmown_Click(object sender, RoutedEventArgs e)
        {
            if (band.Visible == true)
            {
                //AreaFreqOnKnown?.Invoke(band.ValueBegin, band.ValueEnd);
                OnFreqArea?.Invoke(FrequencyType.Known, band.ValueBegin, band.ValueEnd);
            }
            else
            {
                if (CursorX.Visible == true && CursorY.Visible == true)
                    OnFreqArea?.Invoke(FrequencyType.Known, cursorX, cursorX);
            }
        }

        private void setImport_Click(object sender, RoutedEventArgs e)
        {
            if (band.Visible == true)
            {
                //AreaFreqOnImportant?.Invoke(band.ValueBegin, band.ValueEnd);
                OnFreqArea?.Invoke(FrequencyType.Important, band.ValueBegin, band.ValueEnd);
            }
            else
            {
                if (CursorX.Visible == true && CursorY.Visible == true)
                    OnFreqArea?.Invoke(FrequencyType.Important, cursorX, cursorX);
            }
        }


        List<DoubleValueTuple> ListForbiddenFreqs = new List<DoubleValueTuple>();
        List<DoubleValueTuple> ListKnownFreqs = new List<DoubleValueTuple>();
        List<DoubleValueTuple> ListImportantFreqs = new List<DoubleValueTuple>();

        public class DoubleValueTuple
        {
            public double FreqStart;
            public double FreqEnd;

            public DoubleValueTuple(double FreqStart, double FreqEnd)
            {
                this.FreqStart = FreqStart;
                this.FreqEnd = FreqEnd;
            }
        }

        public void PaintSpecBands(byte freqType, double[] FreqStartMHz, double[] FreqEndMHz)
        {
            switch (freqType)
            {
                case 0:
                    ListForbiddenFreqs.Clear();
                    for (int i = 0; i < Math.Min(FreqStartMHz.Count(), FreqEndMHz.Count()); i++)
                    {
                        ListForbiddenFreqs.Add(new DoubleValueTuple(FreqStartMHz[i], FreqEndMHz[i]));
                    }
                    break;
                case 1:
                    ListKnownFreqs.Clear();
                    for (int i = 0; i < Math.Min(FreqStartMHz.Count(), FreqEndMHz.Count()); i++)
                    {
                        ListKnownFreqs.Add(new DoubleValueTuple(FreqStartMHz[i], FreqEndMHz[i]));
                    }
                    break;
                case 2:
                    ListImportantFreqs.Clear();
                    for (int i = 0; i < Math.Min(FreqStartMHz.Count(), FreqEndMHz.Count()); i++)
                    {
                        ListImportantFreqs.Add(new DoubleValueTuple(FreqStartMHz[i], FreqEndMHz[i]));
                    }
                    break;
                default: return;
            }
            RepaintBands();
        }

        private void RepaintBands()
        {
            if (chart.ViewXY.Bands.Count > 2)
                chart.ViewXY.Bands.RemoveRange(2, chart.ViewXY.Bands.Count - 1);

            List<Band> bands = new List<Band>();

            for (int i = 0; i < ListForbiddenFreqs.Count(); i++)
            {
                Band band = new Band();
                band.MouseInteraction = false;
                band.MouseHighlight = MouseOverHighlight.None;
                band.Fill.GradientFill = GradientFill.Solid;
                band.Fill.Color = Color.FromArgb(63, 255, 0, 0);
                band.ValueBegin = ListForbiddenFreqs[i].FreqStart;
                band.ValueEnd = ListForbiddenFreqs[i].FreqEnd;
                bands.Add(band);
            }

            for (int i = 0; i < ListKnownFreqs.Count(); i++)
            {
                Band band = new Band();
                band.MouseInteraction = false;
                band.MouseHighlight = MouseOverHighlight.None;
                band.Fill.GradientFill = GradientFill.Solid;
                band.Fill.Color = Color.FromArgb(63, 0, 255, 0);
                band.ValueBegin = ListKnownFreqs[i].FreqStart;
                band.ValueEnd = ListKnownFreqs[i].FreqEnd;
                bands.Add(band);
            }

            for (int i = 0; i < ListImportantFreqs.Count(); i++)
            {
                Band band = new Band();
                band.MouseInteraction = false;
                band.MouseHighlight = MouseOverHighlight.None;
                band.Fill.GradientFill = GradientFill.Solid;
                //band.Fill.Color = Color.FromArgb(63, 255, 165, 0);
                band.Fill.Color = Color.FromArgb(63, Colors.Khaki.R, Colors.Khaki.G, Colors.Khaki.B);

                band.ValueBegin = ListImportantFreqs[i].FreqStart;
                band.ValueEnd = ListImportantFreqs[i].FreqEnd;
                bands.Add(band);
            }

            chart.ViewXY.Bands.AddRange(bands);
        }

        public enum CRR
        {
            CRRX,
            CRRX2
        }
        public void CursorCRRChange(double FreqMHz, double BwMHz, CRR crrx = CRR.CRRX)
        {
            switch (crrx)
            {
                case CRR.CRRX:
                    {
                        CursorCRR.ValueAtXAxis = FreqMHz;
                        BandCRR.ValueBegin = FreqMHz - BwMHz / 2d;
                        BandCRR.ValueEnd = FreqMHz + BwMHz / 2d;
                    }
                    break;
                case CRR.CRRX2:
                    {
                        CursorCRR2.ValueAtXAxis = FreqMHz;
                        BandCRR2.ValueBegin = FreqMHz - BwMHz / 2d;
                        BandCRR2.ValueEnd = FreqMHz + BwMHz / 2d;
                    }
                    break;
            }
        }
        #endregion

        #region ChartHandlers

        Point posDown;
        private void chart_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                //if (CursorX.Visible == true && CursorY.Visible == true)
                {
                    posDown = e.GetPosition(chart);
                }
            }
        }

        private bool posUpDown(Point posUp, Point posDown)
        {
            if (posDown == posUp)
                return true;

            if ((posDown.X - 1 <= posUp.X) && (posDown.X + 1 >= posUp.X) && (posDown.Y - 1 <= posUp.Y) && (posDown.Y + 1 >= posUp.Y))
                return true;
            else return false;
        }

        Point posUp;
        private void chart_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (CursorX.Visible == true && CursorY.Visible == true)
                {
                    var posCheck = e.GetPosition(chart);
                    var marginsRect = chart.ViewXY.GetMarginsRect();

                    if ((posCheck.X >= marginsRect.X) && (posCheck.X <= marginsRect.X + marginsRect.Width) && (posCheck.Y >= marginsRect.Y) && (posCheck.Y <= marginsRect.Y + marginsRect.Height))
                    {
                        posUp = e.GetPosition(chart);
                        //if (posDown == posUp)
                        if (posUpDown(posUp, posDown))
                        {
                            var pos = e.GetPosition(chart);
                            xAxis.CoordToValue((int)pos.X, out var xValue, true);
                            yAxis.CoordToValue((int)pos.Y, out var yValue, true);

                            CursorX.ValueAtXAxis = xValue;
                            CursorY.Value = yValue;

                            CursorOnFreq?.Invoke(xValue);

                            string freq = "F";
                            //string MHz = "МГц";
                            string level = "U";
                            //string dB = "дБ";

                            tbFreq.Visibility = Visibility.Visible;
                            tbFreq.Text = String.Format(freq + " = " + "{0} " + MHz, xValue.ToString("F3"));
                            tbLevel.Visibility = Visibility.Visible;
                            tbLevel.Text = String.Format(level + " = " + "{0} " + dB, yValue.ToString("F1"));
                        }
                    }
                }
                else
                {
                    var posCheck = e.GetPosition(chart);
                    var marginsRect = chart.ViewXY.GetMarginsRect();

                    if ((posCheck.X >= marginsRect.X) && (posCheck.X <= marginsRect.X + marginsRect.Width) && (posCheck.Y >= marginsRect.Y) && (posCheck.Y <= marginsRect.Y + marginsRect.Height))
                    {
                        posUp = e.GetPosition(chart);
                        //if (posDown == posUp)
                        if (posUpDown(posUp, posDown))
                        {
                            var pos = e.GetPosition(chart);
                            xAxis.CoordToValue((int)pos.X, out var xValue, true);
                            yAxis.CoordToValue((int)pos.Y, out var yValue, true);

                            CursorX.ValueAtXAxis = xValue;
                            CursorY.Value = yValue;

                            double widthOfBand = Math.Abs(band.ValueEnd - band.ValueBegin);

                            band.ValueBegin = xValue - (widthOfBand / 2d);
                            band.ValueEnd = xValue + (widthOfBand / 2d);
                        }
                    }
                }
            }
        }

        public void UpdateCursorLabel()
        {
            string freq = "F";
            //string MHz = "МГц";
            string level = "U";

            double xValue = CursorX.ValueAtXAxis;
            double yValue = CursorY.Value;
            tbFreq.Text = String.Format(freq + " = " + "{0} " + MHz, xValue.ToString("F3"));
            tbLevel.Text = String.Format(level + " = " + "{0} " + dB, yValue.ToString("F1"));
        }

        private void chart_MouseClick(object sender, MouseButtonEventArgs e)
        {
            //if (e.ChangedButton == MouseButton.Left)
            //{
            //    //if (CursorX.Visible == true && CursorY.Visible == true)
            //    {
            //        var pos = e.GetPosition(chart);
            //        xAxis.CoordToValue((int)pos.X, out var xValue, true);
            //        yAxis.CoordToValue((int)pos.Y, out var yValue, true);

            //        CursorX.ValueAtXAxis = xValue;
            //        CursorY.Value = yValue;

            //        CursorOnFreq?.Invoke(xValue);

            //        string freq = "Частота";
            //        string MHz = "МГц";
            //        string level = "Уровень";
            //        string dB = "дБ";

            //        tbFreq.Visibility = Visibility.Visible;
            //        tbFreq.Text = String.Format(freq + ": " + "{0} " + MHz, xValue.ToString("F4"));
            //        tbLevel.Visibility = Visibility.Visible;
            //        tbLevel.Text = String.Format(level + ": " + "{0} " + dB, yValue.ToString("F1"));
            //    }
            //}
        }

        private bool isDoubleClick = false;

        private void chart_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.LeftButton == MouseButtonState.Pressed)
            {
                isDoubleClick = !isDoubleClick;
                if (isDoubleClick)
                {
                    var pos = e.GetPosition(chart);
                    xAxis.CoordToValue((int)pos.X, out var xValue, true);

                    CursorOrBandVisible(false);

                    double rangeWidth = xAxis.Maximum - xAxis.Minimum;

                    //band.ValueBegin = xValue - (rangeWidth / 6d);
                    //band.ValueEnd = xValue + (rangeWidth / 6d);
                    band.ValueBegin = xValue - (rangeWidth / 30d);
                    band.ValueEnd = xValue + (rangeWidth / 30d);
                }
                else
                {
                    CursorOrBandVisible(true);

                    ExBearingOff();
                }
            }
        }

        private void CursorOrBandVisible(bool value)
        {
            if (value)
            {
                CursorX.Visible = true;
                CursorY.Visible = true;

                tbFreq.Visibility = Visibility.Visible;
                tbLevel.Visibility = Visibility.Visible;

                tbBl.Visibility = Visibility.Collapsed;
                tbBw.Visibility = Visibility.Collapsed;
                tbBr.Visibility = Visibility.Collapsed;

                band.Visible = false;
            }
            else
            {
                CursorX.Visible = false;
                CursorY.Visible = false;

                tbFreq.Visibility = Visibility.Collapsed;
                tbLevel.Visibility = Visibility.Collapsed;

                tbBl.Visibility = Visibility.Visible;
                tbBw.Visibility = Visibility.Visible;
                tbBr.Visibility = Visibility.Visible;

                band.Visible = true;
            }
        }

        private void Band_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            string Bl = "Bl";
            string Bw = "Bw";
            string Br = "Br";
            //string MHz = "МГц";

            tbBl.Text = String.Format(Bl + " = " + "{0} " + MHz, e.NewBeginValue.ToString("F3"));

            double width = Math.Abs(e.NewEndValue - e.NewBeginValue);

            tbBw.Text = String.Format(Bw + " = " + "{0} " + MHz, width.ToString("F3"));

            tbBr.Text = String.Format(Br + " = " + "{0} " + MHz, e.NewEndValue.ToString("F3"));
        }

        private void UpdateBandValue()
        {
            string Bl = "Bl";
            string Bw = "Bw";
            string Br = "Br";
            //string MHz = "МГц";

            tbBl.Text = String.Format(Bl + " = " + "{0} " + MHz, band.ValueBegin.ToString("F3"));

            double width = Math.Abs(band.ValueEnd - band.ValueBegin);

            tbBw.Text = String.Format(Bw + " = " + "{0} " + MHz, width.ToString("F3"));

            tbBr.Text = String.Format(Br + " = " + "{0} " + MHz, band.ValueEnd.ToString("F3"));
        }

        private void xAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.NewMax - e.NewMin > _GlobalRangeXmax - _GlobalRangeXmin)
            {
                SetOriginScale();
                RangeChangeEvent?.Invoke(xAxis.Minimum, xAxis.Maximum);
                outBandPictureBoxIndex(xAxis.Minimum, xAxis.Maximum, out int startindex, out int endindex);

                SendIndexesToDrawActiveRectangleOnNbar?.Invoke(this, new DoubleIntEventArgs(startindex, endindex));
                //nBar.DrawActiveRectangle(startindex, endindex);

                return;
            }

            if (e.NewMax - e.NewMin < _GlobalBandWidthMHz / 100)
            {
                double center = (xAxis.Maximum + xAxis.Minimum) / 2d;
                xAxis.SetRange(center - _GlobalBandWidthMHz / 100d / 2d, center + _GlobalBandWidthMHz / 100d / 2d);
            }

            if (e.NewMax > _GlobalRangeXmax)
            {
                double shift = e.NewMax - _GlobalRangeXmax;
                xAxis.SetRange(e.NewMin - shift, e.NewMax - shift);
            }

            if (e.NewMin < _GlobalRangeXmin)
            {
                double shift = _GlobalRangeXmin - e.NewMin;
                xAxis.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            RangeChangeEvent?.Invoke(xAxis.Minimum, xAxis.Maximum);
            outBandPictureBoxIndex(xAxis.Minimum, xAxis.Maximum, out int startIndex, out int endIndex);

            SendIndexesToDrawActiveRectangleOnNbar?.Invoke(this, new DoubleIntEventArgs(startIndex, endIndex));
            //nBar.DrawActiveRectangle(startIndex, endIndex);

            GenerateCustomTicks(ref xAxis);

            ClearPersistentLayerV1();
            ClearPersistentLayerV2();
            //ClearPersistentLayerV3();
        }

        private void yAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.NewMax - e.NewMin > _GlobalRangeYmax - _GlobalRangeYmin)
            {
                SetOriginScale();
                return;
            }

            if (e.NewMax > _GlobalRangeYmax)
            {
                double shift = e.NewMax - _GlobalRangeYmax;
                yAxis.SetRange(e.NewMin - shift, e.NewMax - shift);
            }

            if (e.NewMin < _GlobalRangeYmin)
            {
                double shift = _GlobalRangeYmin - e.NewMin;
                yAxis.SetRange(e.NewMin + shift, e.NewMax + shift);
            }

            ClearPersistentLayerV1();
            ClearPersistentLayerV2();
            //ClearPersistentLayerV3();
        }

        private void ConstantLine_ValueChanged(object sender, Arction.Wpf.SemibindableCharting.SeriesXY.ValueChangedEventArgs e)
        {
            if (e.NewValue > _GlobalRangeYmax)
            {
                RRLine.Value = _GlobalRangeYmax;
                return;
            }
            if (e.NewValue < _GlobalRangeYmin)
            {
                RRLine.Value = _GlobalRangeYmin;
                return;
            }
            _Threshold = (int)e.NewValue;
            tbThres.Text = String.Format(thres + " = " + "{0} " + dB, _Threshold.ToString("F0"));
            ThresholdChange?.Invoke((int)e.NewValue);
        }
        #endregion

        private void UpdateThresValue()
        {
            tbThres.Text = String.Format(thres + " = " + "{0} " + dB, _Threshold.ToString("F0"));
        }

        bool flagKey = false;
        private void chart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Y)
            {
                if (flagKey == false)
                {
                    if (pointSeries.Points != null)
                    {
                        var data = pointSeries.Points;

                        var start = MinVisibleX;
                        var end = MaxVisibleX;

                        double[] dataArray = new double[data.Count()];

                        List<double> LDX = new List<double>();
                        List<double> LDY = new List<double>();

                        for (int i = 0; i < data.Count(); i++)
                        {
                            if (data[i].X >= start && data[i].X <= end)
                            {
                                LDX.Add(data[i].X);
                                LDY.Add(data[i].Y);
                            }
                        }
                        if (LDX.Count > 1)
                        {
                            double minValue = LDY.Min<double>();
                            if (minValue < _GlobalRangeYmin) minValue = _GlobalRangeYmin;
                            double maxValue = LDY.Max<double>();
                            if (maxValue > _GlobalRangeYmax) maxValue = _GlobalRangeYmax;
                            SetYRange(minValue, maxValue);
                        }
                    }
                }
                else
                {
                    SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
                }
                flagKey = !flagKey;
            }

            if (e.Key == Key.Space)
            {
                control_Click(this, null);
            }

            if (e.Key == Key.W)
            {
                thresUp_Click(this, null);
            }

            if (e.Key == Key.S)
            {
                thresDown_Click(this, null);
            }

            if (e.Key == Key.A)
            {
                moveLeft_Click(this, null);
            }

            if (e.Key == Key.D)
            {
                moveRight_Click(this, null);
            }

            if (e.Key == Key.Q)
            {
                qbearing_Click(this, null);
            }

            if (e.Key == Key.E)
            {
                bearing_Click(this, null);
            }

            if (e.Key == Key.R)
            {
                radioSuppr_Click(this, null);
            }

            if (e.Key == Key.F)
            {
                radioSupprFHSS_Click(this, null);
            }

            if (e.Key == Key.T)
            {
                target_Click(this, null);
            }

            if (e.Key == Key.Z)
            {
                setImport_Click(this, null);
            }

            if (e.Key == Key.X)
            {
                setForb_Click(this, null);
            }

            if (e.Key == Key.C)
            {
                setKmown_Click(this, null);
            }

        }


    }
}

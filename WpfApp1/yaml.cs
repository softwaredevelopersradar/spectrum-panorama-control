﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace WpfApp1
{
    public partial class MainWindow
    {
        public VColors YamlLoad()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader("LocalVColors.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localVColors = new VColors();
            try
            {
                localVColors = deserializer.Deserialize<VColors>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (localVColors == null)
            {
                localVColors = GenerateDefaultLocalVColors();
                YamlSave(localVColors);
            }
            return localVColors;
        }

        private VColors GenerateDefaultLocalVColors()
        {
            var localVColors = new VColors();
            return localVColors;
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        public void YamlSave(VColors localVColors)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(localVColors);

                using (StreamWriter sw = new StreamWriter("LocalVColors.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }


    public class VColors
    {
        public VColors()
        {
            allColors = new AllColors();
        }

        public AllColors allColors { get; set; }
    }

    public class AllColors
    {
        public AllColors()
        {
            SpectrumColor = new cColor();
            SpectrumColor2 = new cColor();
            ThresholdColor = new cColor();
            CursorColor = new cColor();
            BackGroudColor = new cColor();
            MarkupColor = new cColor();
        }

        public cColor SpectrumColor { get; set; }
        public cColor SpectrumColor2 { get; set; }
        public cColor ThresholdColor { get; set; }
        public cColor CursorColor { get; set; }
        public cColor BackGroudColor { get; set; }
        public cColor MarkupColor { get; set; }
}

    public class cColor
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}
